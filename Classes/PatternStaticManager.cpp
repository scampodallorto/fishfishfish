//
//  PatternStaticManager.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#include "PatternStaticManager.h"
#include "GameSprite.h"

PatternStaticManager::PatternStaticManager(frw::res::managerBatch* managerBatch, Pattern* pattern, float height) : PatternManager(managerBatch, pattern, height) {
    
    // crea il primo pattern
    addNewPattern();
    
    const int wscreen = 480; // frw::dsp::screen::getInstance()->width();
    
    do {
        
        // ora controlla se abbiamo bisogon di altri pattern. i pattern statici partono da destra verso sinistra
        Pattern* lastPattern = _patterns.back();
        GameSprite* sprite = lastPattern->getResource();
        CCPoint pt = sprite->position();
        int wsprite = lastPattern->getSize().width;
        if(pt.x + wsprite / 2 < wscreen) {
            
            addNewPattern(); // aggiungiamo un altro pattern
            continue;
        }
        else {
            break;
        }
        
    }while(1);
}