//
//  SceneFake.h
//  OZ
//
//  Created by Sara Scarazzolo on 05/03/13.
//
//

#pragma once

#include <frw/common.h>

class SceneFakeToMain : public frw::core::scene
{
public:
    SceneFakeToMain(const char* identifier);
    virtual ~SceneFakeToMain();
    
    virtual void transitionFinish();
    
    DECLARATE_SCENE(SceneFakeToMain);
    
};
