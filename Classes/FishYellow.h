//
//  FishYellow.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 13/02/14.
//
//

#ifndef __FishFishFish__FishYellow__
#define __FishFishFish__FishYellow__

#include <frw/common.h>

#include "Fish.h"

class FishYellow : public Fish {
    
public:
    virtual ~FishYellow() {
        
    }
    
protected:
    FishYellow(frw::res::manager* layer, float speedUp, float speedDown);
    
public:
    static FishYellow* create(frw::res::manager* layer) {
        
        return new FishYellow(layer, 350, 200);
    }
    
};

#endif /* defined(__FishFishFish__FishYellow__) */
