//
//  BonusBird.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 12/02/14.
//
//

#ifndef __FlappyKitten__BonusBird__
#define __FlappyKitten__BonusBird__

#include <frw/common.h>

class GameSprite;

class BonusPoints {
    
    
public:
    BonusPoints();
    
    virtual ~BonusPoints();
    
    void update(float dt);
    
    GameSprite* _sprite;
    
    frw::math::PathFloat _heightPath;
};

#endif /* defined(__FlappyKitten__BonusBird__) */
