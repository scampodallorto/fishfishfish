//
//  Obstacles04.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__Obstacles04__
#define __FlappyKitten__Obstacles04__

#include "Obstacles01a.h"

class Obstacles04a : public Obstacles01a {
    
public:
    Obstacles04a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FlappyKitten__Obstacles04__) */
