//
//  PatternManager.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#include "PatternManager.h"
#include "Pattern.h"
#include "GameSprite.h"

PatternManager::PatternManager(frw::res::managerBatch* batch, Pattern* pattern, float height) : _managerBatch(batch), _forceHeight(height), _priority(frw::res::overlaybase::kPriorityNormal), _show(true), _alpha(1.0f) {
    
    pattern->initialize(batch, ccp(0,0));
    pattern->getResource()->priority(_priority);
    pattern->visible(false);
    _patternsSource.push_back(pattern);
}

PatternManager::~PatternManager() {
    
    for(int i = 0; i < _patternsSource.size(); ++i) {
        
        SAFE_DELETE(_patternsSource[i]);
    }
    
    for(std::list<Pattern*>::iterator it = _patterns.begin(); it != _patterns.end(); ++it) {
        
        SAFE_DELETE(*it);
    }

    _patterns.clear();
}

void PatternManager::addPattern(Pattern* pattern) {
        
    pattern->initialize(_managerBatch, ccp(0,0));
    pattern->getResource()->priority(_priority);
    pattern->visible(false);
    _patternsSource.push_back(pattern);
}

void PatternManager::setPriority(float priority) {
    _priority = priority;
    for(std::list<Pattern*>::iterator it = _patterns.begin(); it != _patterns.end(); ++it) {
        
        Pattern* p = *it;
        p->getResource()->priority(priority);
        
    }
}

void PatternManager::setShow(bool show) {
    
    _show = show;
    
    for(std::list<Pattern*>::iterator it = _patterns.begin(); it != _patterns.end(); ++it) {
        
        Pattern* p = *it;
        p->getResource()->visible(show);
        
    }
}

void PatternManager::setAlpha(float a) {
    
    _alpha = a;
    
    for(std::list<Pattern*>::iterator it = _patterns.begin(); it != _patterns.end(); ++it) {
        
        Pattern* p = *it;
        p->getResource()->alpha(_alpha);
        
    }
}

void PatternManager::changePattern(Pattern* pattern) {
    
    _patternsSource.clear();
    pattern->initialize(_managerBatch, ccp(0,0));
    pattern->getResource()->priority(_priority);
    pattern->getResource()->visible(false);
    _patternsSource.push_back(pattern);
}

void PatternManager::addNewPattern() {
    
    if(_patternsSource.size() > 0) {
        
        int index = frw::math::random::Range(0, (_patternsSource.size()*1000)-1)/1000;
        Pattern* p = _patternsSource[index]->clone();
        CCPoint pt;
        
        p->getResource()->priority(_priority);
        p->getResource()->visible(_show);
        p->getResource()->alpha(_alpha);

        if(_patterns.size() > 0) {
            // posiziona subito dopo il pattern precedente
            Pattern* last = _patterns.back();
            GameSprite* prePatter = last->getResource();
            
            pt = prePatter->position();
            
            pt.x += last->getSize().width / 2;
            
            pt.x += p->getResource()->size().width / 2;
            
            pt.x -= 1.0f;
        }
        else {
            // posiziona come primo
            if(_forceHeight > 0) {
                pt = ccp(p->getSize().width / 2, _forceHeight);
            }
            else {
                pt = ccp(p->getSize().width / 2, 320 - p->getSize().height/2);
            }
        }
        
        p->getResource()->position(pt);
        
        _patterns.push_back(p);
    }
}

Pattern* PatternManager::getFirstPattern() {
    
    if(_patterns.size() > 0) {
        return *_patternsSource.begin();
    }
    
    return NULL;
}

const std::list<Pattern*>& PatternManager::getListPatterns() {
    
    return _patterns;
}




