//
//  PatternHScrollManager.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__PatternHScrollManager__
#define __GoPenguin__PatternHScrollManager__

#include <frw/common.h>

#include "Pattern.h"
#include "PatternManager.h"
#include "PatternStaticManager.h"

/**
 * \class PatternHScrollListener
 * \brief mi dice a che distanza sono arrivato
 */
class PatternHScrollListener {
    
public:
    /** ho finito i pattern */
    virtual void onFinishedPatterns() {}
    
    /** ritorna la distanza percorsa */
    virtual void onPatternsDistance(float &distance) {}

    /** arrivato alla fine */
    virtual void onPatternsStopped() {}
};

/**
 * \class PatternHScrollManager
 * \brief create a pattern scroll manager. scroll pattern with horizontal move
 */
class PatternHScrollManager : public PatternStaticManager {
   
protected:
    /**
     * ctor
     */
    PatternHScrollManager(float speed, frw::res::managerBatch* managerBatch, Pattern* pattern, float height = -1) : PatternStaticManager(managerBatch, pattern, height), _speed(speed), _distance(0), _generatePatterns(true), _state(kIdle), _counterPattern(-1), _lastPatternWithStop(NULL) {
        
    }
    
public:
    /**
     * aggiorna pattern dinamico. lo scroll controlla va da destra verso sinistra e controlla che l'ultimo sia sempre fuori schermo, se nn lo e' allora aggiunge un pattern
     */
    virtual void update(float dtime);
    
public:
    
    /**
     * crea manager
     */
    static PatternHScrollManager* create(float speedScroll, frw::res::managerBatch* managerBatch, Pattern* pattern, float height = -1) {
        
        return new PatternHScrollManager(speedScroll, managerBatch, pattern, height);
        
    }
    
public:
    
    /**
     * fai partire calcolo distanza
     */
    inline void start(int patternCount) {
        
        _counterPattern = patternCount;
        _start = true;
        _generatePatterns = true;
        _state = kIdle;
        _distance = 0;
    }
    
    inline void stop(Pattern* lastPattern = NULL, CCPoint position = ccp(0,0)) {
        
        _start = false;
        _generatePatterns = false;
    }
    
    /**
     * la forzatura ferma lo scroll
     */
    inline void stopScroll() {
       
        _state = kStopped;
    }
    
    inline bool isStart() {
        
        return _start;
    }
    
    /**
     * torna la distanza percorsa
     */
    inline float distance() {
        return _distance;
    }
    
    inline void addListener(PatternHScrollListener* listener) {
        _listener.push_back(listener);
    }
    
    inline void removeListener(PatternHScrollListener* listener) {
        
        std::vector<PatternHScrollListener*>::iterator it = std::find(_listener.begin(), _listener.end(), listener);
        
        if(it != _listener.end()) {
           _listener.erase(it);
        }
    }
    
    inline Pattern* getLastPatternWithStop() {
        
        return _lastPatternWithStop;
    }
    
private:
    
    enum State {
        kIdle,
        kStopped
    };
    
    State _state;
    
    /**< velocità scroll */
    const float _speed;
    
    /**< distanza percorsa */
    float _distance;
    
    /**< fai partice calcolo distanza */
    bool _start;
    
    /**< fa in modo di generare patterns
     * se a false, l'ultimo patterns si ferma a centro schermo
     */
    bool _generatePatterns;    

    /**< pattern massimi che posson passare */
    int _counterPattern;
    
    /** questo e' l'utlimo patter quando abbiamo fatto stop */
    Pattern* _lastPatternWithStop;

    /**
     * listener, serve per quando scrolliamo per sapere che distanza sto percorrendo
     */
    std::vector<PatternHScrollListener*> _listener;
};

#endif /* defined(__GoPenguin__PatternHScrollManager__) */
