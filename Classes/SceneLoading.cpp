//
//  SceneMenu.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 10/11/12.
//
//

#include "SceneLoading.h"
#include <frw/common.h>
#include <frw/core/director.h>
#include <frw/dsp/screen.h>
#include <frw/res/menu.h>

CREATE_SCENE(SceneLoading);

#define TIME_REPEAT_MUSIC (5.0f)

#define kMaxLogos (1)

const char* logos_[kMaxLogos] = {
    "Loading.png"
};

SceneLoading::SceneLoading(const char* identifier) : frw::core::scene(identifier), _status(kStatusShowLogo), _index(0)
{
    
}

SceneLoading::~SceneLoading()
{
    SAFE_RELEASE_MANAGER(_manager);
    SAFE_RELEASE_OBJECT(_background);
}

void SceneLoading::initialize()
{
    _INFO("LOAD SceneLoading::initialize()");
    _manager = new frw::res::manager("sprite manager loading", 255, 155, 0);
    push(_manager);
    

    _background = frw::res::sprite::create(logos_[_index]);
    _background->alpha(0);
    _background->scale(GAMESCALE);
    _manager->addChild(_background);
    _background->disableBlackBand();
    _background->position(240, 160);

    _to_game = 1.0f;
    
    _alpha.push(0.0f);
    _alpha.push(1.0f);
    _alpha.time(2.0f);
    _alpha.run();
    _INFO("LOADED SceneLoading::initialize()");
}

void SceneLoading::finalize()
{
    SAFE_RELEASE_OBJECT(_background);
    
    SAFE_RELEASE_MANAGER(_manager);
    
    _INFO(CCTextureCache::sharedTextureCache()->description());
    CCTextureCache::purgeSharedTextureCache();
}

void SceneLoading::update(float dt)
{

    switch(_status)
    {
        case kStatusShowLogo:
        {
            float a = _alpha.update(dt);
            
            _background->alpha(a);
            
            if(_alpha.done())
            {
                _status = kStatusWaitLogo;
                
                _to_game = 1.0f;
            }
            
            break;
        }
            
        case kStatusWaitLogo:
        {
            _to_game -= dt;
            
            if(_to_game < 0.0f)
            {
                _to_game = 1.0f;
                _status = kStatusHideLogo;
                
                
                if(_index+1 >= kMaxLogos)
                {
                    _status = kStatusToGame;
                    break;
                }
                
                _alpha.reset();
                _alpha.push(1.0f);
                _alpha.push(0.0f);
                _alpha.time(2.0f);
                _alpha.run();
            }
            
            break;
        }

        case kStatusHideLogo:
        {
            float a = _alpha.update(dt);
            
            _background->alpha(a);
            
            if(_alpha.done())
            {
                _index++;
                _manager->removeChild(_background, false);
                SAFE_RELEASE_OBJECT(_background);
                
                _background = frw::res::sprite::create(logos_[_index]);
                _background->position(0, 0);
                _background->alpha(0);
                _manager->addChild(_background);
                _background->position(frw::dsp::screen::getInstance()->width()/2.0f, frw::dsp::screen::getInstance()->height()/2.0f);

                _alpha.reset();
                _alpha.push(0.0f);
                _alpha.push(1.0f);
                _alpha.time(1.0f);
                _alpha.run();
                
                _status = kStatusShowLogo;
            }
            
            break;
        }

        case kStatusNextLogo:
        {
            break;
        }
            
        case kStatusToGame:
        {
            if(_to_game > 0)
            {
                _to_game -= dt;
                if(_to_game < 0)
                {
                    frw::core::director::getInstance()->setNextScene("SceneMain", frw::core::director::kTransitionProgressRadialCW);
                    //frw::core::director::getInstance()->setNextScene("SceneGame", frw::core::director::kTransitionCrossFade);
                }
            }
            break;
        }
    }
}



