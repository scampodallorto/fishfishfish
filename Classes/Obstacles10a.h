//
//  Obstacles10a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles10a__
#define __FishFishFish__Obstacles10a__

#include "Obstacles01a.h"

class Obstacles10a : public Obstacles01a {
    
public:
    Obstacles10a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles10a__) */
