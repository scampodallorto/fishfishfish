//
//  Kitten.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#include "Fish.h"
#include "Sounds.h"

Fish::Fish(frw::res::manager* layer, float speedUp, float speedDown) : kSpeedUp(speedUp), kSpeedDown(speedDown) {
    
    _ohoh = GameSprite::create("End.png");
    
    _ohoh->visible(false);
    
    _ohoh->priority(frw::res::overlaybase::kPriorityHigh+2);
    
    layer->push(_ohoh);
    
    _state = kStateIdle;
    
    _gravity = 0;
    
    _start = false;
    
    _eat = false;
    
    _speed = 0;
    
    _rotationMin = 360-20;
    _rotationMax = 360+60;
    _rotation = 360;
}

Fish::~Fish() {
    
    SAFE_RELEASE_OBJECT(_sprite);
    SAFE_RELEASE_OBJECT(_ohoh);
}

void Fish::position(float x, float y) {
    
    _sprite->position(x, y);
}

void Fish::pathPosition(std::vector<CCPoint> &args, float time, bool loop, CCCallFunc * func, float delay) {
    
    _sprite->pathPosition(args, time, loop, func, delay);
    
}

void Fish::pathPositionX(std::vector<float> &args, float time, bool loop, CCCallFunc * func, float delay) {
    
    _sprite->pathPositionX(args, time, loop, func, delay);
    
}

void Fish::pathPositionY(std::vector<float> &args, float time, bool loop, CCCallFunc * func, float delay) {
    
    _sprite->pathPositionY(args, time, loop, func, delay);
    
}

bool Fish::isPathPosition() {
    
    return _sprite->isPathPosition();
}

bool Fish::isPathPositionX() {
    
    return _sprite->isPathPositionX();
}

bool Fish::isPathPositionY() {
    
    return _sprite->isPathPositionX();
}

CCSize Fish::size() {
    
    return _sprite->size();
}

void Fish::idle() {
    
    _sprite->playAnimation("idle", true);
    
}

void Fish::eat() {
    _eat = true;
    frw::snd::manager::getInstance()->play(kSfx_TakeBonus);
}

void Fish::up() {

    _start = true;

    // spara in alto il gatto
    _accelerationPath.reset();
    _accelerationPath.push(90);
    _accelerationPath.push(270);
    _accelerationPath.time(1.3f);
    _accelerationPath.run();
    
    _rotationPath.reset();
    _rotationPath.push(_rotation);
    _rotationPath.push(_rotationMin);
    _rotationPath.time(1.1f);
    _rotationPath.run();
    
    _speed = 0;
    
    frw::snd::manager::getInstance()->play(kSfx_Jump);
}

void Fish::dead() {
    
    _sprite->playAnimation("dead");
    
    _ohoh->visible(true);
    
    _ohoh->position(_sprite->position().x + _sprite->size().width/2 + _ohoh->size().width / 2, _sprite->position().y - _sprite->size().height/2);
    
    _state = kStateDeadStart;
    
    _timeToDead = 1.0f;
    
    frw::snd::manager::getInstance()->play(kSfx_Dead);
}

CCRect Fish::collisionBox() {

    CCRect rc = _sprite->collisionBox();
    rc.size.width = rc.size.width*0.9;
    rc.size.height = rc.size.height*0.9;
    
    return rc;
}

void Fish::visibility(bool v) {
    
    _sprite->visible(v);
    
}

CCPoint Fish::position() {
    
    return _sprite->position();
}

void Fish::update(float dt) {

    
    switch (_state) {
            
        case kStateIdle: {
            
            float t = _accelerationPath.update(dt);
            float r = _rotationPath.update(dt);
            
            if(!_accelerationPath.done()) {
                
                const double pi = 2*3.1415926535;
                const float rad = pi / 360.0f;
                float a = -sinf(t*rad);
                  
                _gravity = a;
            }
            else {
                //if(strcmp(_sprite->getCurrentAnimation()->name_, "down") != 0 && _start && !_eat)
                //    _sprite->playAnimation("down", true);
                ;
            }
            
            if(_start) {
                
                CCPoint pt = _sprite->position();
                _speed += dt * kSpeedUp * 2.0f;
                
                if(_gravity > 0) {
                    pt.y += (_gravity * dt) * _speed;
                    //_rotation = r;
                }
                else {
                    pt.y += (_gravity * dt) * kSpeedDown;
                }
                
                if(pt.y < size().height/2+50) {
                    pt.y = size().height/2+50;
                }

                if(_rotationPath.done()) {
                    _rotation += kSpeedDown * dt * 2.0f;
                    if(_rotation > _rotationMax) {
                        _rotation = _rotationMax;
                    }
                }
                else {
                    _rotation = r;
                }
                
                _sprite->position(pt);
            }
            
            if(_eat) {
                if(_sprite->isAnimationFinished()) {
                    _sprite->playAnimation("down", true);
                    _eat = false;
                    return;
                }
            }
            
            break;
        }
            
        case kStateDeadStart: {
            
            _timeToDead -= dt;
            if(_timeToDead < 0) {
                _timeToDead = 0;
                _ohoh->visible(false);
                _state = kStateDeadWait;
                frw::snd::manager::getInstance()->play(kSfx_Fall);
            }
            
            break;
        }
            
        case kStateDeadWait: {
            
            CCPoint pt = _sprite->position();
            pt.y += (kSpeedDown*2) * dt;
            _sprite->position(pt);
            
            _rotation += kSpeedDown * dt;
            if(_rotation > _rotationMax) {
                _rotation = _rotationMax;
            }
            
            if(pt.y > 480 + _sprite->size().height) {
                _state = kStateDeadDone;
            }
            
            break;
        }
            
        case kStateDeadDone: {
            
            
            break;
        }

        default:
            break;
    }

    
     _sprite->rotation(_rotation);
}




