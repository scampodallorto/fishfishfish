//
//  Obstacles02.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__Obstacles02__
#define __FlappyKitten__Obstacles02__

#include "Obstacles01a.h"

class Obstacles02a : public Obstacles01a {
    
public:
    Obstacles02a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FlappyKitten__Obstacles02__) */
