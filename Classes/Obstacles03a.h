//
//  Obstacles03.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__Obstacles03__
#define __FlappyKitten__Obstacles03__

#include "Obstacles01a.h"

class Obstacles03a : public Obstacles01a {
    
public:
    Obstacles03a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FlappyKitten__Obstacles03__) */
