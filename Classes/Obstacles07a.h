//
//  Obstacles07a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles07a__
#define __FishFishFish__Obstacles07a__

#include "Obstacles01a.h"

class Obstacles07a : public Obstacles01a {
    
public:
    Obstacles07a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles07a__) */
