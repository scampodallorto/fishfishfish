//
//  ObstaclesManager.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#include "ObstaclesManager.h"
#include "Obstacles.h"
#include "Obstacles01a.h"
#include "Obstacles02a.h"
#include "Obstacles03a.h"
#include "Obstacles04a.h"
#include "Obstacles05a.h"
#include "Obstacles06a.h"
#include "Obstacles07a.h"
#include "Obstacles08a.h"
#include "Obstacles09a.h"
#include "Obstacles10a.h"
#include "Obstacles11a.h"
#include "Obstacles12a.h"
#include "Obstacles13a.h"
#include "Obstacles14a.h"
#include "Obstacles15a.h"
#include "Fish.h"
#include "SceneMain.h"
#include "GameSprite.h"
#include "BonusPoints.h"
#include "SeaBubble.h"

#include <frw/fx/particles.h>

#define kTimeToNextObstacles (.8f)
#define kTimeToNextBird (frw::math::random::Range(2, 3))
#define kTimeToNextFishBubble (frw::math::random::Range(1, 2))
//#define kTimeToNextBird (1)

#if defined(DEBUG)
    // #define DEBUG_DISABLE_COLLISION
#endif

/**
 * ctor
 */
ObstaclesManager::ObstaclesManager() : frw::res::manager("Obstacles layer manager"), _obstacles(std::vector<Obstacles*>()), _obstaclesSources(std::vector<Obstacles*>()), _start(false), _toNextObstacles(0) , _timeToAddBubbleFish(0) {
    
    addSource(new Obstacles01a());
    addSource(new Obstacles02a());
    addSource(new Obstacles03a());
    addSource(new Obstacles04a());
    addSource(new Obstacles05a());
    addSource(new Obstacles06a());
    addSource(new Obstacles07a());
    addSource(new Obstacles08a());
    addSource(new Obstacles09a());
    addSource(new Obstacles10a());
    addSource(new Obstacles11a());
    addSource(new Obstacles12a());
    addSource(new Obstacles13a());
    addSource(new Obstacles14a());
    addSource(new Obstacles15a());

    Obstacles::_timeToChange = kTimeToChange;
    Obstacles::_idObstacles = 0;
    
    _toNextBird = kTimeToNextBird;
    _timeToAddBubbleFish = kTimeToNextFishBubble;
}

/**
 * dtor
 */
ObstaclesManager::~ObstaclesManager() {
    
    for(int i = 0; i < _obstaclesSources.size(); ++i) {
        
        Obstacles* o = _obstaclesSources[i];
        SAFE_DELETE(o);
    }
    
    for(int i = 0; i < _obstacles.size(); ++i) {
        
        Obstacles* o = _obstacles[i];
        SAFE_DELETE(o);
    }

    for(int i = 0; i < _bonus.size(); ++i) {
        
        BonusPoints* o = _bonus[i];
        SAFE_DELETE(o);
    }

    for(int i = 0; i < _bubblesFish.size(); ++i) {
        
        FishBubble* o = _bubblesFish[i];
        SAFE_DELETE(o);
    }

    for(int i = 0; i < _takeBonus.size(); ++i) {
        
        GameSprite* o = _takeBonus[i];
        SAFE_RELEASE_OBJECT(o);
    }
}

void ObstaclesManager::addSource(Obstacles* source) {
    
    _obstaclesSources.push_back(source);
    source->visible(false);
}

void ObstaclesManager::addObstacles() {
    
    if(_obstaclesSources.size() == 0)
        return;
    
    int index = frw::math::random::Range(0, (_obstaclesSources.size()*1000)-1) / 1000;
    
    //index = _obstaclesSources.size()-1;
    
    
    Obstacles* source = _obstaclesSources[index];
    Obstacles* o = source->clone();
    o->push(this);
    _obstacles.push_back(o);
    
    Obstacles01a* ob = (Obstacles01a*)o;
    
    if(!ob->_up) {
        return;
    }
    
    CCPoint pt = ob->_up->position();
    int wOb = ob->_up->size().width;
    int hOb = ob->_up->size().height;
    
    pt.x += wOb * 2;

    if(pt.y + (hOb / 2)> 100) {
        _toNextBird--;
        if(_toNextBird <= 0) {
            _toNextBird = kTimeToNextBird;
            BonusPoints* bonus = new BonusPoints();
            _bonus.push_back(bonus);
            
            push(bonus->_sprite);

            pt.y += (hOb / 2) - bonus->_sprite->size().height / 2;
            bonus->_sprite->position(pt);

            // mettiamo l'uccelino subito dopo la colonna
            
            bonus->_heightPath.reset();
            bonus->_heightPath.push(pt.y);
            bonus->_heightPath.push(pt.y + bonus->_sprite->size().height*0.2f);
            bonus->_heightPath.push(pt.y);
            bonus->_heightPath.loop(true);
            bonus->_heightPath.run();
        }
    }
}

void ObstaclesManager::start() {

    _start = true;
}

void ObstaclesManager::stop() {
    
    _start = false;
}

void ObstaclesManager::clearObstacles() {
    
    
    for(int i = 0; i < _obstacles.size(); i++) {
        
        Obstacles* o = _obstacles[i];
        SAFE_DELETE(o);
    }
    
    for(int i = 0; i < _bonus.size(); i++) {
        
        BonusPoints* bird = _bonus[i];
        SAFE_DELETE(bird);
    }

    for(int i = 0; i < _takeBonus.size(); i++) {
        
        GameSprite* o = _takeBonus[i];
        SAFE_RELEASE_OBJECT(o);
    }

    _obstacles.clear();
    _bonus.clear();
}

/**
 * update
 */
void ObstaclesManager::update(Fish* fish, float dt) {
    
    if(!_start) {
        frw::res::manager::update(dt);
        return;
    }
    
    Obstacles* lastObstacle = NULL;
    if(_obstacles.size() > 0) {
        lastObstacle = _obstacles[_obstacles.size()-1];
    }
    
    
    if(lastObstacle) {

        if(lastObstacle->position().x < 480 - lastObstacle->size().width/2) {
            _toNextObstacles += dt;
            if(_toNextObstacles > kTimeToNextObstacles) {
                _toNextObstacles = 0;
                addObstacles();
            }
        }
    }
    else {
        _toNextObstacles += dt;
        if(_toNextObstacles > kTimeToNextObstacles) {
            _toNextObstacles = 0;
            addObstacles();
        }
    }
    
    _timeToAddBubbleFish -= dt;
    if(_timeToAddBubbleFish < 0) {
        _timeToAddBubbleFish = kTimeToNextFishBubble;
        FishBubble* b = new FishBubble("Bubble_Small.png", ccp(fish->position().x + fish->size().width/2, fish->position().y));
        push(b->_sprite);
        _bubblesFish.push_back(b);
    }
    
    for(int i = _bubblesFish.size()-1; i >= 0; --i) {
        
        FishBubble* o = _bubblesFish[i];
        if(o->update(dt)) {
            SAFE_DELETE(o);
            _bubblesFish.erase(_bubblesFish.begin()+i);
        }
        
    }

    for(int i = _takeBonus.size()-1; i >= 0; --i) {
        
        GameSprite* o = _takeBonus[i];
        CCPoint pt = o->position();
        pt.x -= kObstaclesSpeed*dt;
        o->position(pt);
        if(!o->isPathAlpha()) {
            SAFE_RELEASE_OBJECT(o);
            _takeBonus.erase(_takeBonus.begin()+i);
        }
        
    }
    
    for(int i = _bonus.size()-1; i >= 0; --i) {
        
        BonusPoints* bonus = _bonus[i];
        
        CCRect rectKitten = fish->collisionBox();
        CCRect rcBird = bonus->_sprite->collisionBox();
        if(rcBird.intersectsRect(rectKitten)) {
            
            //frw::res::particle::emitterquad *p = frw::res::particle::emitterquad::create(this, "piume.plist", bonus->_sprite->position());
            //ccTexParams params  = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE};
            //p->getTexture()->setTexParameters(&params);
            
            GameSprite* takeBonus = GameSprite::create("SeaStar_Take.png");
            _takeBonus.push_back(takeBonus);
            takeBonus->position(bonus->_sprite->position());
            push(takeBonus);
            
            std::vector<float> argsS;
            argsS.push_back(1.0f);
            argsS.push_back(1.5f);
            takeBonus->pathScale(argsS, .7f);

            std::vector<float> argsA;
            argsA.push_back(1.0f);
            argsA.push_back(1.5f);
            takeBonus->pathAlpha(argsA, .5f);

            ApplicationSendMessage("MAIN", SceneMain::kMessageEarnPoint);
            SAFE_DELETE(bonus);
            _bonus.erase(_bonus.begin()+i);
            fish->eat();
            
            
            continue;
        }
        
        CCPoint pt = bonus->_sprite->position();
        pt.x -= kObstaclesSpeed*dt;
        bonus->_sprite->position(pt);
        if(pt.x < -bonus->_sprite->size().width) {
            SAFE_DELETE(bonus);
            _bonus.erase(_bonus.begin()+i);
        }
    }
    
    for(int i = _obstacles.size()-1; i >= 0; --i) {
        
        Obstacles* o = _obstacles[i];

        if(o->position().x < 240) {
#if defined(DEBUG_DISABLE_COLLISION)
#else
            if(o->isTouch(fish->collisionBox())) {
                
                ApplicationSendMessage("MAIN", SceneMain::kMessageDead);
                
                _start = false;
                
                break;
            }
#endif
        }
        
        if(o->update(dt)) {
            
            SAFE_DELETE(o);
            _obstacles.erase(_obstacles.begin()+i);
            
        }
    }

    frw::res::manager::update(dt);
}

