//
//  PatternManager.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__PatternManager__
#define __GoPenguin__PatternManager__

#include <frw/common.h>


class Pattern;

/**
 * \class PatternManager
 * \brief class manager to managment a pattern class
 */
class PatternManager {
  
protected:
    /**
     *  ctor
     */
    PatternManager(frw::res::managerBatch* managerBatch, Pattern* pattern, float height = -1);
    
public:
    /**
     * dtor
     */
    virtual ~PatternManager();
    
    /**
     * aggiorna
     */
    virtual void update(float dtime) = 0;
    
    /**
     * aggiungi un altro pattern
     */
    virtual void addPattern(Pattern* pattern);
    
    /**
     * cambia pattern
     */
    virtual void changePattern(Pattern* pattern);
    
    /**
     * torna il primo pattern
     */
    virtual Pattern* getFirstPattern();
    
    /**
     * priorità
     */
    virtual void setPriority(float);
    
    /**
     * visualizza o meno i pattern (continuano a essere generati pero')
     */
    virtual void setShow(bool);
    
    /**
     * setta alpha
     */
    virtual void setAlpha(float a);
    
    /**
     * torna lista di patterns
     */
    virtual const std::list<Pattern*>& getListPatterns();
    
protected:
    /**
     * add pattern to list
     */
    void addNewPattern();
    
protected:
    
    float _forceHeight;
    
    float _priority;
    
    float _alpha;
    
    bool _show;
    
    frw::res::managerBatch* _managerBatch;
    
    /**
     * pattern corrente
     */
    std::vector<Pattern*> _patternsSource;
    
    /**
     * pattern a schermo
     */
    std::list<Pattern*> _patterns;
};


#endif /* defined(__GoPenguin__PatternManager__) */
