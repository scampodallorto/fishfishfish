//
//  Kitten.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#ifndef __FlappyKitten__Kitten__
#define __FlappyKitten__Kitten__

#include <frw/common.h>
#include "GameSprite.h"

class Fish {
    
    
public:
    Fish(frw::res::manager* layer, float speedUp, float speedDown);
    virtual ~Fish();

    virtual CCRect collisionBox();
    
    void position(float x, float y);

    CCPoint position();
    
    void pathPosition(std::vector<CCPoint> &args, float time, bool loop = false, CCCallFunc * func = NULL, float delay = -1);
    void pathPositionX(std::vector<float> &args, float time, bool loop = false, CCCallFunc * func = NULL, float delay = -1);
    void pathPositionY(std::vector<float> &args, float time, bool loop = false, CCCallFunc * func = NULL, float delay = -1);

    bool isPathPosition();
    bool isPathPositionX();
    bool isPathPositionY();

    CCSize size();
    
    void idle();

    void up();

    void dead();
    
    void update(float dt);
    
    void eat();
    
    void visibility(bool );
public:
    
    inline bool isDead() {
        
        return _state == kStateDeadDone;
    }
    
    inline GameSprite* sprite() {
        
        return _sprite;
    }
    
    
protected:
    enum State {
        kStateIdle,
        kStateDeadStart,
        kStateDeadWait,
        kStateDeadDone
    };
    
    State _state;
    
    const float kSpeedUp;
    const float kSpeedDown;
    
    bool _eat;
    bool _start;
    
    // velocita corrent Y
    float _gravity;
    
    // velocità viene calcolata come cos(0 - 90) * t
    float _speed;
    
    bool _dead;
    
    float _timeToDead;
    
    GameSprite *_ohoh;
    
    GameSprite *_sprite;
    
    // l'accelerazione funziona con funzione sin(90 - 270) * t
    frw::math::PathFloat _accelerationPath;
    frw::math::PathFloat _rotationPath;
    
    // rotazione
    float _rotation;
    float _rotationMax;
    float _rotationMin;
};

#endif /* defined(__FlappyKitten__Kitten__) */
