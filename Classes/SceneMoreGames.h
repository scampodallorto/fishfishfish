//
//  SceneMoreGames.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#ifndef __FlappyKitten__SceneMoreGames__
#define __FlappyKitten__SceneMoreGames__

#include <frw/common.h> 

class GameSprite;

class SceneMoreGames : public frw::res::menuPopup {
    
    
public:
    SceneMoreGames();
    virtual ~SceneMoreGames();
    
protected:
    GameSprite* _skin;
    frw::res::button* _buttonExit;
    frw::res::button* _buttonGames;
    frw::res::button* _buttonSite;
    frw::res::button* _buttonFacebook;
};


#endif /* defined(__FlappyKitten__SceneMoreGames__) */

