//
//  WorldPattern.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 07/02/14.
//
//

#ifndef __GoPenguin__WorldPattern__
#define __GoPenguin__WorldPattern__

#include <frw/common.h>

#define kMaxWorlds (2)

struct WorldPatter {
    
    std::string _name;
    std::vector<std::string> _background;
    std::vector<std::string> _floor;
    
    static int _count;
    
    static int _patternsPerLevel;
    
    static std::vector<WorldPatter> _worlds;
    
    static void loadFromXML(const char* file);
};

#endif /* defined(__GoPenguin__WorldPattern__) */
