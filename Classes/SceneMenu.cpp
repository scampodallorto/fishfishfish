//
//  SceneMenu.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//
#include "SceneMain.h"
#include "SceneMenu.h"
#include "Fish.h"
#include "FishRed.h"
#include "FishYellow.h"
#include "FishViolet.h"
#include "Sounds.h"

class ButtonPlay : public frw::res::button {

public:
    ButtonPlay() : frw::res::button("Button_Play.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {
        
        frw::snd::manager::getInstance()->play(kSfx_Tap);
        
        ApplicationSendMessage("MENU", SceneMenu::kMessagePlay);
    }
    
};

class ButtonLeaderboard : public frw::res::button {
    
public:
    ButtonLeaderboard() : frw::res::button("Button_Leaderboard.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {

        frw::snd::manager::getInstance()->play(kSfx_Tap);

        ApplicationSendMessage("MENU", SceneMenu::kMessageLeaderboard);
    }
    
};

class ButtonMoreGames : public frw::res::button {
    
public:
    ButtonMoreGames() : frw::res::button("Button_More.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {
        
        frw::snd::manager::getInstance()->play(kSfx_Tap);

        ApplicationSendMessage("MAIN", SceneMain::kMessageMoreGames);
    }
    
};

SceneMenu::SceneMenu() : frw::res::menuPopup("main menu"), _fish(NULL), _fish1Text(NULL), _fish2Text(NULL), _buttonStart(NULL), _buttonLeaderboard(NULL), _buttonMore(NULL), _timeToFish(0), _stop(false)  {
    
    RegisterMessageListener("MENU");
    
    disableBackground();
    
    _buttonStart = new ButtonPlay();
    push(_buttonStart);
    _buttonStart->position(160, 200);
    
    _buttonLeaderboard = new ButtonLeaderboard();
    push(_buttonLeaderboard);
    _buttonLeaderboard->position(_buttonStart->position().x + (_buttonStart->size().width * GAMESCALE) + 10, _buttonStart->position().y);

    _buttonMore = new ButtonMoreGames();
    push(_buttonMore);
    _buttonMore->position(_buttonLeaderboard->position().x + ((_buttonLeaderboard->size().width * GAMESCALE) / 2 + 10) + (_buttonMore->size().width * GAMESCALE) / 2, _buttonStart->position().y + (_buttonStart->size().height * GAMESCALE) / 2 - (_buttonMore->size().height * GAMESCALE) / 2);

    _fish1Text = frw::res::manager::createSpriteByFrame("Title_Tiny.png");
    _fish2Text = frw::res::manager::createSpriteByFrame("Title_Fish.png");
    
    _fish1Text->scale(GAMESCALE);
    _fish2Text->scale(GAMESCALE);
    
    _fish1Text->priority(frw::res::overlaybase::kPriorityNormal+1);
    _fish2Text->priority(frw::res::overlaybase::kPriorityNormal+1);
    
    std::vector<CCPoint> argsP;
    argsP.push_back(ccp(230 - (_fish1Text->size().width*GAMESCALE)/2,90));
    argsP.push_back(ccp(230 - (_fish1Text->size().width*GAMESCALE)/2,70));
    argsP.push_back(ccp(230 - (_fish1Text->size().width*GAMESCALE)/2,90));
    _fish1Text->pathPosition(argsP, 3.0f, true);
    
    argsP.clear();
    argsP.push_back(ccp(250 + (_fish2Text->size().width * GAMESCALE) / 2, 70));
    argsP.push_back(ccp(250 + (_fish2Text->size().width * GAMESCALE) / 2, 90));
    argsP.push_back(ccp(250 + (_fish2Text->size().width * GAMESCALE) / 2, 70));
    _fish2Text->pathPosition(argsP, 3.0f, true);

    push(_fish1Text);
    push(_fish2Text);
    
    _timeToFish = 0.0f;
    
    _fish = NULL;
}

SceneMenu::~SceneMenu() {
    
    
    UnRegisterMessageListener();
    
    SAFE_RELEASE_OBJECT(_buttonStart);
    SAFE_RELEASE_OBJECT(_buttonLeaderboard);
    SAFE_RELEASE_OBJECT(_buttonMore);
    SAFE_RELEASE_OBJECT(_fish1Text);
    SAFE_RELEASE_OBJECT(_fish2Text);
    
}

CREATE_MESSAGE_LISTENER(SceneMenu) {
    
    switch (uiMessage) {
            
        case kMessagePlay: {

            ApplicationSendMessage("MAIN", SceneMain::kMessagePlay);
            
            break;
        }
            
        case kMessageLeaderboard: {
            
#if defined(__APPLE__)
            frw::platform::SocialSystem_LeaderboardShow("com.smallthinggame.tinyfish.records2");
#else
            frw::platform::SocialSystem_LeaderboardShow("13636");
#endif
            
            break;
        }
            
        default:
            break;
    }
    
}


void SceneMenu::hide() {
    
    _stop = true;
    
    frw::res::menuPopup::hide();
}


void SceneMenu::update(float dt) {
    
    SAFE_PTR(_fish)->update(dt);
    
    if(_fish) {
        if(!_fish->isPathPositionX()) {
            SAFE_DELETE(_fish);
        }
    }
    
    if(_stop) {
        frw::res::manager::update(dt);
        return;
    }
    
    if(!_fish) {
        
        _timeToFish -= dt;
        if(_timeToFish < 0) {
            _timeToFish = frw::math::random::Range(3.0f, 5.0f);

            int r = frw::math::random::Range(0, 200);
            if(r < 30) {
                _fish = FishRed::create(this);
            }
            else if(r < 60) {
                _fish = FishYellow::create(this);
            }
            else {
                _fish = FishViolet::create(this);
            }
            
            std::vector<float> argsX;
            std::vector<float> argsY;
            
            float y = frw::math::random::Range(_fish->size().height+50, 280 - _fish->size().height);
            CCPoint pt1 = ccp(-_fish->size().width, y);
            CCPoint pt2 = ccp(480+_fish->size().width, y);
            argsX.push_back(pt1.x);
            argsX.push_back(pt2.x);
            
            argsY.push_back(pt1.y);
            argsY.push_back(pt1.y + 10.0f);
            argsY.push_back(pt1.y);
            
            _fish->pathPositionX(argsX, 10.0f);
            _fish->pathPositionY(argsY, 3.0f, true);
            
            _fish->idle();
        }
    }
    
    frw::res::manager::update(dt);
}


