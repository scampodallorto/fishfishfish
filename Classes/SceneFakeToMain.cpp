//
//  SceneFake.cpp
//  OZ
//
//  Created by Sara Scarazzolo on 05/03/13.
//
//

#include "SceneFakeToMain.h"

CREATE_SCENE(SceneFakeToMain);

SceneFakeToMain::SceneFakeToMain(const char* identifier) : frw::core::scene("SceneFakeToMain")
{
    
}

SceneFakeToMain::~SceneFakeToMain()
{
    
}

void SceneFakeToMain::transitionFinish()
{
    frw::core::director::getInstance()->setNextScene("SceneMain", frw::core::director::kTransitionProgressRadialCCW, 1.0f, _userdata);
}
