//
//  Obstacles.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__Obstacles__
#define __FlappyKitten__Obstacles__

#include <frw/common.h>

#define kTimeToChange (8)
#define kObstaclesSpeed (165)


class GameSprite;

class Obstacles {
    
public:
    Obstacles() { _type = -1; }
    virtual ~Obstacles() {}
    
    /** inserisci nella scena */
    virtual void push(frw::res::manager* manager) = 0;
    
    /** l'ostacolo e' stato toccato da sto sprite? */
    virtual bool isTouch(const CCRect &rect) = 0;
    
    /** sposta ostacoli */
    virtual bool update(float ftime) = 0;
    
    /** clona l'ostacolo */
    virtual Obstacles* clone() = 0;

    virtual void visible(bool enable) = 0;
    
    virtual CCPoint position() = 0;
    
    virtual CCSize size() = 0;
    
    static int _timeToChange;
    static int _idObstacles;
    
    int _type;
};

#endif /* defined(__FlappyKitten__Obstacles__) */
