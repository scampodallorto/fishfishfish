//
//  SceneMenu.h
//  BubHole
//
//  Created by Sara Scarazzolo on 10/11/12.
//
//

#ifndef __BubHole__SceneLoading__
#define __BubHole__SceneLoading__

#include <iostream>

#include <frw/core/scene.h>
#include <frw/res/sprite.h>
//#include <frw/res/font.h>
#include <frw/res/manager.h>
#include <frw/message/message.h>
#include <frw/callback/callback.h>
#include <frw/math/path.h>

class SceneLoading : public frw::core::scene
{
public:
    SceneLoading(const char* identifier);
    virtual ~SceneLoading();
    
    virtual void initialize();
    virtual void finalize();
    virtual void update(float dt);
    
    DECLARATE_SCENE(SceneLoading);
    
private:
    frw::res::manager* _manager;
    frw::res::sprite* _background;
    
    frw::math::PathAlpha _alpha;
    
    enum Status
    {
        kStatusShowLogo,
        kStatusWaitLogo,
        kStatusHideLogo,
        kStatusNextLogo,
        kStatusToGame
    };
    
    Status _status;
    int _index;
    float _to_game;
};


#endif /* defined(__BubHole__SceneMenu__) */
