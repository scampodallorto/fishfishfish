//
//  Obstacles06a.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#include "Obstacles06a.h"

Obstacles06a::Obstacles06a() {
    
    _type = 6;
    
    if(Obstacles::_timeToChange < 0) {
        //
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    GameSprite* o = GameSprite::create("Background_Sea_1.png");
    
    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);
    
    _up = GameSprite::create("Obstacle_Mid.png");
    _down = GameSprite::create("Obstacle_Mid.png");

    CCPoint ptUp;
    ptUp.x = 480+_down->size().width;
    _down->position(480+_down->size().width, 320-o->size().height/2-_down->size().height);
    
    ptUp.x = frw::math::random::Range(_down->size().height*2, _down->position().y - _down->size().height*3);
    
    _up->position(ptUp);
    
    SAFE_RELEASE_OBJECT(o);
    

}

/** clona questo oggetto */
Obstacles* Obstacles06a::clone() {
    
    return new Obstacles06a();
}