//
//  Obstacles08a.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#include "Obstacles08a.h"

Obstacles08a::Obstacles08a() {
    
    _type = 8;
    
    if(Obstacles::_timeToChange < 0) {
        //
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    GameSprite* o = GameSprite::create("Background_Sea_1.png");
    
    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);

    _up = GameSprite::create("Obstacle_Mid.png");
    _down = GameSprite::create("Obstacle_Mid.png");
    
    CCPoint pt1;
    
    pt1.x = 480+_down->size().width;
    pt1.y = _up->size().height/6;
        _down->position(pt1.x, 320-o->size().height-_down->size().height/2);
    
    CCPoint pt2;
    pt2 = pt1;
    pt2.y += _up->size().height/3.5f;
    
        _up->position(pt1);
    
    std::vector<float> args;
    args.push_back(pt1.y);
    args.push_back(pt2.y);
    args.push_back(pt1.y);
    
    _up->pathPositionY(args, 2.0f, true);
    
    SAFE_RELEASE_OBJECT(o);
    

    _x = 480+_down->size().width/2;

}

/** clona questo oggetto */
Obstacles* Obstacles08a::clone() {
    
    return new Obstacles08a();
}