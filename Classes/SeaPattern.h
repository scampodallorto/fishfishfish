//
//  SeaPattern.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 07/02/14.
//
//

#ifndef __GoPenguin__SeaPattern__
#define __GoPenguin__SeaPattern__

#include "FloorPattern.h"

class SeaBubble;

/**
 * \class SeaPattern
 * \brief in questo patternn inseriamo le bolle
 */
class SeaPattern : public FloorPattern {
    
public:
    /**
     * ctor
     */
    SeaPattern(const char* framePattern, const char* frameAlternative = NULL);

    virtual ~SeaPattern();
    
    /**
     * clona questo pattern
     */
    virtual Pattern* clone();
    
private:
    
    virtual void update(float dt);
    
private:
    void addBubble();
    
private:
    float _timebubble;
    
    std::vector<SeaBubble*> _bubblesList;
};

#endif /* defined(__GoPenguin__SeaPattern__) */
