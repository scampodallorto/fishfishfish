//
//  FishYellow.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 13/02/14.
//
//

#include "FishYellow.h"


FishYellow::FishYellow(frw::res::manager* layer, float speedUp, float speedDown) : Fish(layer, speedUp, speedDown) {
    
    _sprite = GameSprite::create("Yellow_Swim_Frame1.png");
    _sprite->addAnimationFromPlist("idle", "yellow_idle.plist");
    _sprite->addAnimationFromPlist("dead", "yellow_dead.plist");
    
    _sprite->priority(frw::res::overlaybase::kPriorityHigh+1);
    
    layer->push(_sprite);
    
    _sprite->position(-999, -999);
    
    _sprite->playAnimation("idle", true);
    
}
