//
//  Obstacles12a.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#include "Obstacles12a.h"

Obstacles12a::Obstacles12a() {
    
    _type = 12;
    
    if(Obstacles::_timeToChange < 0) {
        //
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    GameSprite* o = GameSprite::create("Background_Sea_1.png");

    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);

    _up = GameSprite::create("Obstacle1_Hor.png");
    _down = GameSprite::create("Obstacle1_Hor.png");
    
    if(frw::math::random::Range(0, 100) < 50) {
        _up->position(480+_down->size().width/2, o->size().height*2);
        _down->position(480+_down->size().width/2, 320-o->size().height*2);
    }
    else {
        _up->position(480+_down->size().width/2, o->size().height*2.5);
        _down->position(480+_down->size().width/2, 320-o->size().height*1.5);
    }
    
    SAFE_RELEASE_OBJECT(o);
    

    _x = 480+_down->size().width/2;
}

/** clona questo oggetto */
Obstacles* Obstacles12a::clone() {
    
    return new Obstacles12a();
}