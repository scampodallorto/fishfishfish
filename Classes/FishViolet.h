//
//  FishViolet.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 13/02/14.
//
//

#ifndef __FishFishFish__FishViolet__
#define __FishFishFish__FishViolet__

#include <frw/common.h>

#include "Fish.h"

class FishViolet : public Fish {
    
public:
    virtual ~FishViolet() {
        
    }
    
protected:
    FishViolet(frw::res::manager* layer, float speedUp, float speedDown);
    
public:
    static FishViolet* create(frw::res::manager* layer) {
        
        return new FishViolet(layer, 250, 250);
    }
};

#endif /* defined(__FishFishFish__FishViolet__) */
