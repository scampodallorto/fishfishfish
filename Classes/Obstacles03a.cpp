//
//  Obstacles03.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#include "Obstacles03a.h"

Obstacles03a::Obstacles03a() {
    
    _type = 3;

    CCPoint pt1 = _up->position();
    CCPoint pt2 = _down->position();
    
    int h = _up->size().height * 0.15f;
    
    pt1.y -= h;
    pt2.y -= h;
    
    _up->position(pt1);
    _down->position(pt2);
    
}

/** clona questo oggetto */
Obstacles* Obstacles03a::clone() {
    
    return new Obstacles03a();
}