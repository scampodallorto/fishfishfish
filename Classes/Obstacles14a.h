//
//  Obstacles14a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 15/02/14.
//
//

#ifndef __FishFishFish__Obstacles14a__
#define __FishFishFish__Obstacles14a__

#include "Obstacles01a.h"

class Obstacles14a : public Obstacles01a {
    
public:
    Obstacles14a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles14a__) */
