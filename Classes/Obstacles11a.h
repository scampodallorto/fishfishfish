//
//  Obstacles11a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles11a__
#define __FishFishFish__Obstacles11a__

#include "Obstacles01a.h"

class Obstacles11a : public Obstacles01a {
    
public:
    Obstacles11a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles11a__) */
