//
//  Obstacles12a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles12a__
#define __FishFishFish__Obstacles12a__

#include "Obstacles01a.h"

class Obstacles12a : public Obstacles01a {
    
public:
    Obstacles12a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles12a__) */
