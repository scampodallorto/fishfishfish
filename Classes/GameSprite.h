//
//  GameSprite.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__GameSprite__
#define __GoPenguin__GameSprite__

#include <frw/common.h>

/**
 * \class GameSprite
 * \brief sprite di gioco dove viene moltiplicato direttamente il valore di scala
 */
class GameSprite : public frw::res::sprite {
  
public:
    /**
     * ctor
     */
    GameSprite(const char*);
    
    virtual ~GameSprite();
    
    /**
     * crea uno sprite
     */
    static GameSprite* create(const char* frame);

    virtual void position(CCPoint);

    virtual const CCPoint position();

    virtual void position(float x, float y);

    virtual CCRect collisionBox();

    
public:
    /**
     * return size scalated
     */
    virtual const CCSize size();

    /**
     * set scale
     */
    virtual void scale(float s);
    
    /**
     * return scale
     */
    virtual const float scale();
};

#endif /* defined(__GoPenguin__GameSprite__) */
