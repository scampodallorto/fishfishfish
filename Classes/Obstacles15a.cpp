//
//  Obstacles15a.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 15/02/14.
//
//

#include "Obstacles15a.h"

Obstacles15a::Obstacles15a() {
    
    _type = 15;
    
    if(Obstacles::_timeToChange < 0) {
        //
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    GameSprite* o = GameSprite::create("Background_Sea_1.png");
    
    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);

    switch(_idObstacles) {
        case 0: {
            _up = GameSprite::create("Obstacle1.png");
            break;
        }
        case 1: {
            _up = GameSprite::create("Obstacle2.png");
            break;
        }
        case 2: {
            _up = GameSprite::create("Obstacle3.png");
            break;
        }
        case 3: {
            _up = GameSprite::create("Obstacle4.png");
            break;
        }
    }
    
    Obstacles::_timeToChange--;
    if(Obstacles::_timeToChange < 0) {
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
        _idObstacles++;
        if(_idObstacles > 3) {
            _idObstacles = 0;
        }
    }
    
    _down = GameSprite::create("Obstacle4_Hor.png");
    
    _up->position(480+_down->size().width/2, 320-o->size().height*0.1);
    _down->position(480+_down->size().width/2, o->size().height*4);
    
    SAFE_RELEASE_OBJECT(o);
    
    _x = 480+_down->size().width/2;
}

/** clona questo oggetto */
Obstacles* Obstacles15a::clone() {
    
    return new Obstacles15a();
}