//
//  SceneGameOver.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#include "SceneGameOver.h"
#include "SceneMain.h"
#include "Counter.h"
#include "GameSprite.h"
#include "Sounds.h"

class ButtonRation : public frw::res::button {
  
    
public:
    ButtonRation() : frw::res::button("Button_Vote.png") {
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {
        
#if defined(__APPLE__)
        frw::platform::OpenUrl("https://itunes.apple.com/us/app/tiny-fish-under-the-sea/id824701475?mt=8");
#else
        frw::platform::OpenUrl("https://play.google.com/store/apps/details?id=com.smallthing.tinyfish");
#endif
        
    }
    
};


SceneGameOver::SceneGameOver(GameSprite* floor) : frw::res::menuPopup("Layer popup"), _skin(NULL), _game(NULL), _over(NULL), _score(NULL), _currentScore(NULL), _bestScore(NULL), _counterPoints(0), _state(kIdle) {
 
    _skin = GameSprite::create("GameOverRecord.png");
    _skin->priority(frw::res::overlaybase::kPriorityLow);
    
    _game = GameSprite::create("Game.png");
    _over = GameSprite::create("Over.png");
    
    _currentScore = new Counter(this);
    _bestScore = new Counter(this);
    
    int hFloor = floor->size().height;
    int hBackground = _skin->size().height;
    CCPoint pt = ccp(240, 320 - hFloor / 2);
    pt.y -= hBackground/2;
    pt.y -= hFloor/2;
    
    _heightSkinOnScreen = pt.y;
    
    _skin->position(240, _heightSkinOnScreen);
    
    
    _buttonRating = new ButtonRation();
    _buttonRating->alpha(0);
    _buttonRating->position(_skin->position().x + _skin->size().width/2 - _buttonRating->size().width*GAMESCALE / 2, pt.y + _skin->size().height / 2 + 10);
    
    push(_skin);
    push(_game);
    push(_over);
    push(_buttonRating);
    
}

SceneGameOver::~SceneGameOver() {
    
    SAFE_RELEASE_OBJECT(_skin);
    SAFE_RELEASE_OBJECT(_game)
    SAFE_RELEASE_OBJECT(_over);
    SAFE_RELEASE_OBJECT(_buttonRating);
    SAFE_DELETE(_currentScore);
    SAFE_DELETE(_bestScore);
}

void SceneGameOver::show(int score) {

    _score = score;
    _counterPoints = 0;
    _currentBestScore = CCUserDefault::sharedUserDefault()->getIntegerForKey(kKeyBestScore);
    _bestScore->setValue(_currentBestScore);
    _currentScore->setValue(0);
    
    float endY = _heightSkinOnScreen;
    
    std::vector<CCPoint> args;
    args.push_back(ccp(240, 320 + _skin->size().height));
    args.push_back(ccp(240, endY));
    
    _skin->pathPosition(args, .5f);
    _currentScore->position(130, _skin->position().y + _currentScore->height()*0.2f);
    _bestScore->position(320, _skin->position().y + _currentScore->height()*0.2f);


    args.clear();
    args.push_back(ccp(240 - _game->size().width / 2, -_game->size().height));
    args.push_back(ccp(240 - _game->size().width / 2, _game->size().height / 2 + 50));
    _game->pathPosition(args, .5f);

    args.clear();
    args.push_back(ccp(240 + _over->size().width / 2, -_over->size().height));
    args.push_back(ccp(240 + _over->size().width / 2, _over->size().height / 2 + 50));
    _over->pathPosition(args, .5f);

    _state = kWaitShow;
    
    _timeToScore = 0;
}

void SceneGameOver::update(float dt) {
    
    
    switch (_state) {
            
        case kIdle: {
            
            break;
        }
            
        case kWaitShow: {
            
            _currentScore->position(130, _skin->position().y + _currentScore->height()*0.2f);
            _bestScore->position(320, _skin->position().y + _currentScore->height()*0.2f);
            
            if(!_skin->isPathPosition()) {
                
                std::vector<CCPoint> args;
                CCPoint pt1 = _over->position();
                CCPoint pt2 = ccpAdd(pt1, ccp(0,20));
                
                args.clear();
                args.push_back(pt1);
                args.push_back(pt2);
                args.push_back(pt1);
                
                _over->pathPosition(args, 2.0f, true);
                
                pt1 = _game->position();
                pt2 = ccpAdd(pt1, ccp(0,-20));
                args.clear();
                args.push_back(pt1);
                args.push_back(pt2);
                args.push_back(pt1);

                _game->pathPosition(args, 2.0f, true);

                _state = kCountPoint;
                _delayTime = 1.0f;
                
                std::vector<float> argsA;
                argsA.push_back(0.0f);
                argsA.push_back(1.0f);
                _buttonRating->pathAlpha(argsA, 1.0f);
                
                std::vector<cocos2d::ccColor4F> argsC;
                argsC.push_back(ccc4f(1.0f, 1.0f, 1.0f, 1.0f));
                argsC.push_back(ccc4f(.8f, .8f, .8f, 1.0f));
                argsC.push_back(ccc4f(1.0f, 1.0f, 1.0f, 1.0f));
                
                _buttonRating->pathColor(argsC, 2.0f, true);
            }
            
            break;
        }
         
        case kCountPoint: {
            
            _delayTime -= dt;
            if(_delayTime > 0)
                break;
            
            _timeToScore += dt;
            if(_timeToScore > 1.0f/60.0f) {
                
                _timeToScore = 0;
                _counterPoints++;
                
                if(_counterPoints >= _score) {
                    _counterPoints = _score;
                }
                else {
                    frw::snd::manager::getInstance()->play(kSfx_Point);
                }
                
                _currentScore->setValue(_counterPoints);
                
                if(_counterPoints >= _score) {
                    _state = kEnd;
                }
                
                if(_currentBestScore < _counterPoints) {
                    _currentBestScore = _counterPoints;
                    _bestScore->setValue(_currentBestScore);
                }
            }
            
            break;
        }
            
        case kEnd: {
            
            break;
        }
            
        default:
            break;
    }
    
    frw::res::manager::update(dt);
    
}

