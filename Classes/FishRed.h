//
//  FishRed.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 13/02/14.
//
//

#ifndef __FishFishFish__FishRed__
#define __FishFishFish__FishRed__

#include <frw/common.h>

#include "Fish.h"

class FishRed : public Fish {
    
public:
    virtual ~FishRed() {
        
    }
    
protected:
    FishRed(frw::res::manager* layer, float speedUp, float speedDown);
    
public:
    static FishRed* create(frw::res::manager* layer) {
        
        return new FishRed(layer, 300, 300);
    }
    
};

#endif /* defined(__FishFishFish__FishRed__) */
