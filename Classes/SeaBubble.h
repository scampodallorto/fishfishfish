//
//  SeaBubble.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 07/02/14.
//
//

#ifndef __GoPenguin__SeaBubble__
#define __GoPenguin__SeaBubble__

#include <frw/common.h>

class GameSprite;

/**
 * \class Bubble
 * \brief bollicina
 */
class SeaBubble {
    
    friend class SeaPattern;
    
protected:
    /** ctor */
    SeaBubble(const char* frame, float x, float y1, float y2);
    
    /** dtor */
    ~SeaBubble();
    
    /** update bollicina */
    bool update(float dt, CCPoint relativePosition);
    
    /**< position */
    cocos2d::CCPoint _pt;
    
    /**< cocos2dx sprite */
    GameSprite* _sprite;
    
    frw::math::PathFloat _y;
    frw::math::PathAlpha _alpha;
    frw::math::PathFloat _scale;
    
    float _progress;

    float _speedScroll;
    
    CCPoint _position;
};

class FishBubble{

public:
    FishBubble(const char* frame, CCPoint pt);
    
    virtual ~FishBubble();

    bool update(float dt);
    
    GameSprite *_sprite;
    
    float _progress;
};

#endif /* defined(__GoPenguin__SeaBubble__) */
