//
//  FishRed.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 13/02/14.
//
//

#include "FishRed.h"


FishRed::FishRed(frw::res::manager* layer, float speedUp, float speedDown) : Fish(layer, speedUp, speedDown) {
    
    _sprite = GameSprite::create("Red_Swim_Frame1.png");
    _sprite->addAnimationFromPlist("idle", "red_idle.plist");
    _sprite->addAnimationFromPlist("dead", "red_dead.plist");
    
    _sprite->priority(frw::res::overlaybase::kPriorityHigh+1);

    layer->push(_sprite);

    _sprite->position(-999, -999);
    
    _sprite->playAnimation("idle", true);
    
}