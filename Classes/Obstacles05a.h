//
//  Obstacles05.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__Obstacles05__
#define __FlappyKitten__Obstacles05__

#include "Obstacles01a.h"

class Obstacles05a : public Obstacles01a {
    
public:
    Obstacles05a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FlappyKitten__Obstacles05__) */
