//
//  SeaBubble.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 07/02/14.
//
//

#include "SeaBubble.h"
#include "GameSprite.h"

/** ctor */
SeaBubble::SeaBubble(const char* frame, float x, float y1, float y2) : _pt(ccp(0,0)), _sprite(NULL), _progress(0), _y(0) {
    
    _sprite = GameSprite::create(frame);
    _sprite->alpha(0);
    
    float t = 15.5f + frw::math::random::Float() * 5.5f;
    
    _scale.reset();
    _scale.push(0.5f);
    _scale.push(1.5f);
    _scale.time(t);
    _scale.run();
    
    _sprite->priority(frw::res::overlaybase::kPriorityLow+10.0f);
    
    _y.reset();
    _y.push(y1);
    _y.push(y2);
    _y.time(t+1.0f);
    _y.run();
    
    _alpha.reset();
    _alpha.push(1.0f);
    _alpha.push(0.0f);
    _alpha.time(t);
    _alpha.run();
    
    _progress = frw::math::random::Float() * 360.0f;
    
    _position = ccp(x, y1);
    _sprite->position(x, y1);
}

/** dtor */
SeaBubble::~SeaBubble() {
    
    SAFE_RELEASE_OBJECT(_sprite);
}

/** update bollicina */
bool SeaBubble::update(float dt, CCPoint relativePosition) {
    
    _progress += dt;

    relativePosition.x -= 240; // portiamo a 0
    
    _position.x += sin(_progress);

    CCPoint pt = _position;
    pt.x += relativePosition.x;
    
    pt.y = _y.update(dt);
    _sprite->position(pt);
 
    float a = _alpha.update(dt);
    _sprite->alpha(a);
    
    float s = _scale.update(dt);
    _sprite->scale(s);
    
    return _y.done();
}

FishBubble::FishBubble(const char* frame, CCPoint pt) {
    
    _sprite = GameSprite::create(frame);
    _sprite->position(pt);

    std::vector<float> args;
    args.push_back(0.0f);
    args.push_back(1.0f);
    
    _sprite->pathScale(args, 2.1f);
    
    args.clear();
    args.push_back(1.0f);
    args.push_back(.0f);
    
    _sprite->pathAlpha(args, 2.0f);
}

FishBubble::~FishBubble() {
    
}

bool FishBubble::update(float dt) {
    
    _progress += dt;

    CCPoint position = _sprite->position();
    
    position.x += sin(_progress);
    position.y -= dt * 100.0f;
    
    _sprite->position(position);
    
    return !_sprite->isPathAlpha();
}
