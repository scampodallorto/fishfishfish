//
//  WorldPattern.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 07/02/14.
//
//

#include "WorldPattern.h"

int WorldPatter::_count = 0;
std::vector<WorldPatter> WorldPatter::_worlds;
int WorldPatter::_patternsPerLevel;

void WorldPatter::loadFromXML(const char* file) {
    
    TiXmlDocument doc;
    
    frw::file::FILE* f = frw::file::fopen(file, "rt");
    
    if(doc.LoadFile(f->buffer(), f->length())) {
        
        //
        TiXmlElement* root = doc.RootElement();
        if(!root) {
            _WARNING("**** load config: error open file.");
            return;
        }
        
        for(TiXmlNode* world = root->FirstChild("world"); world; world = world->NextSibling("world") ) {
            
            WorldPatter worldPatter;
            
            const char* name = world->ToElement()->Attribute("name");
            if(name) {
                worldPatter._name = name;
            }
            
            for(TiXmlNode* background = world->FirstChild("background"); background; background = background->NextSibling("background")) {
            
                const char* t = background->ToElement()->Attribute("text");
                if(t)
                    worldPatter._background.push_back(t);
            }

            for(TiXmlNode* floor = world->FirstChild("floor"); floor; floor = floor->NextSibling("floor")) {
                
                const char* t = floor->ToElement()->Attribute("text");
                if(t)
                    worldPatter._floor.push_back(t);
            }

            WorldPatter::_worlds.push_back(worldPatter);
        }

        for(TiXmlNode* p = root->FirstChild("patternPerLevel"); p; p = p->NextSibling("patternPerLevel")) {
            
            const char* s = p->ToElement()->Attribute("count");
            if(!s)
                continue;
            CCString v = s;
            WorldPatter::_patternsPerLevel = v.intValue();
        }
        
    }

}
