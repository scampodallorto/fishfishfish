//
//  Counter.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#include "Counter.h"
#include "GameSprite.h"

Counter::Counter(frw::res::manager* manager) : _manager(manager), _value(0) {
    
    for(int i = 0; i < 10; ++i) {
        _number[i] = 0;
    }
    
    GameSprite* s = GameSprite::create("1.png");
    _sizeCharacter = s->size();
    
    SAFE_RELEASE_OBJECT(s);
    
}

Counter::~Counter() {
    
    for(int i = 0; i < 10; ++i) {
        SAFE_RELEASE_OBJECT(_number[i]);
    }
    
}

void Counter::show() {
    
    std::vector<float> args;
    args.push_back(0);
    args.push_back(1.0f);
    
    for(int i = 0; i < 10; ++i) {
        if(_number[i]) {
            _number[i]->pathAlpha(args, 1.0f);
        }
    }
}

void Counter::hide(bool now) {
    
    if(now) {
        for(int i = 0; i < 10; ++i) {
            if(_number[i]) {
                _number[i]->alpha(0);
            }
        }
        return;
    }
    
    std::vector<float> args;
    args.push_back(1.0f);
    args.push_back(.0f);
    
    for(int i = 0; i < 10; ++i) {
        if(_number[i]) {
            _number[i]->pathAlpha(args, 1.0f);
        }
    }
}

void Counter::position(float x, float y) {
   
    int counter = 0;
    int widthString = _sizeCharacter.width * _valueString.length();
    int xP = x + widthString / 2;
    
    for(int i = _valueString.length()-1; i >= 0; --i, counter++) {
        
        _number[counter]->priority(frw::res::overlaybase::kPriorityHigh);
        _number[counter]->position(xP - _sizeCharacter.width*counter, y);
        
        _manager->push(_number[counter]);
        
    }
    
    _position = ccp(x, y);
}

void Counter::setValue(int v) {
    
    _value = v;
    
    char s[256];
    sprintf(s, "%d", v);
    int counter = 0;
    _valueString = s;
    
    for(int i = 0; i < 10; ++i) {
        SAFE_RELEASE_OBJECT(_number[i]);
        _number[i] = NULL;
    }
    
    for(int i = _valueString.length()-1; i >= 0; --i, counter++) {
        
        char c = _valueString[i];
        char file[256];
        sprintf(file, "%c.png", c);
        
        _number[counter] = GameSprite::create(file);
        _number[counter]->priority(frw::res::overlaybase::kPriorityHigh);
        
        _manager->push(_number[counter]);
    }
    
    position(_position.x, _position.y);
}