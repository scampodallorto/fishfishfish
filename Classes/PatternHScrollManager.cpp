//
//  PatternHScrollManager.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//
#include "GameSprite.h"
#include "Pattern.h"
#include "PatternHScrollManager.h"

void PatternHScrollManager::update(float dtime) {
    
    /**
     * come funziona: lo scroll viaggia da destra verso sinistra
     *  quando l'ultimo pattern il suo lato destro e' dentro lo schermo e nn piu' fuori
     *  allora aggiungiamo un pattern. 
     *  Se il primo e' totalmente fuori schermo, allora lo eliminiamo
     */
    
    switch (_state) {
        case kIdle:
            
            // prima aggiorna le posizioni
            for(std::list<Pattern*>::iterator it = _patterns.begin(); it != _patterns.end(); ++it) {
                
                Pattern* pattern = *it;
                pattern->move(dtime * _speed, 0);
            }
            
            if(_generatePatterns) {
                // controlla se sono dentro lo schermo
                
                do {
                    Pattern* last = _patterns.back();
                    CCPoint pt = last->getResource()->position();
                    pt.x += last->getSize().width/2;
                    
                    if(pt.x < 480) {
                        addNewPattern();
                        
                        // counter pattern per vedere quanti ne devono passare
                        if(_counterPattern >= 0) {
                            _counterPattern--;
                            if(_counterPattern < 0) {
                                _start = false;
                                _generatePatterns = false; // finito patterns
                                _lastPatternWithStop = _patterns.back();
                                
                                for(int i = 0; i < _listener.size(); ++i) {
                                    
                                    PatternHScrollListener* listener = _listener[i];
                                    listener->onFinishedPatterns();
                                }
                            }
                        }
                    }
                } while(0);
                
                // controlla se sono fuori schermo
                do {
                    Pattern* first = _patterns.front();
                    if(!first->isOnScreen()) {
                        
                        if(first == _lastPatternWithStop)
                            _lastPatternWithStop = NULL;
                        SAFE_DELETE(first);
                        _patterns.pop_front();
                    }
                }while(0);
            }
            else {
                // controlla se sono fuori schermo
                do {
                    Pattern* first = _patterns.front();
                    if(!first->isOnScreen()) {
                        SAFE_DELETE(first);
                        _patterns.pop_front();
                    }
                }while(0);
                
                // controlliamo l'ultimo pattern se arriva a centro schermo, se arriva a centro schermo fermiamo tutto
                // controlla se sono dentro lo schermo
                do {
                    Pattern* last = _patterns.back();
                    CCPoint pt = last->getResource()->position();
                    pt.x += last->getSize().width/2;
                    
                    if(pt.x < 480) {
                        pt.x = 480 - last->getSize().width / 2;
                        last->getResource()->position(pt);
                        _state = kStopped;
                        
                        for(int i = 0; i < _listener.size(); ++i) {
                            
                            PatternHScrollListener* listener = _listener[i];
                            listener->onPatternsStopped();
                        }

                    }
                }while(0);
            }

            break;
            
        default:
            break;
    }
    
    if(_start && _state == kIdle) {
        _distance += dtime * _speed;
        
        for(int i = 0; i < _listener.size(); ++i) {
            
            PatternHScrollListener* listener = _listener[i];
            listener->onPatternsDistance(_distance);
        }
    }
    
    // update patterns
    for(std::list<Pattern*>::iterator it = _patterns.begin(); it != _patterns.end(); ++it) {
        (*it)->update(dtime);
    }
}