//
//  ObstaclesManager.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__ObstaclesManager__
#define __FlappyKitten__ObstaclesManager__

#include <frw/common.h>

class Obstacles;
class Fish;
class GameSprite;
class BonusPoints;
class FishBubble;

class ObstaclesManager : public frw::res::manager {
    
public:
    /**
     * ctor
     */
    ObstaclesManager();
    
    /**
     * dtor
     */
    virtual ~ObstaclesManager();

    /**
     * update
     */
    virtual void update(Fish *kitten, float dt);
    
    /**
     * aggiungi ostacolo sorgente
     */
    virtual void addSource(Obstacles* source);
    
    /**
     * fai partire gli ostacoli
     */
    virtual void start();
    virtual void stop();
    
    void clearObstacles();
    
protected:
    /**
     * aggiungi ostacolo
     */
    void addObstacles();
    
private:
    
    bool _start;
    
    float _toNextObstacles;
    
    int _toNextBird;
    
    float _timeToAddBubbleFish;
    
    /** ostacoli da dove prendere spunto */
    std::vector<Obstacles*> _obstaclesSources;
    
    std::vector<Obstacles*> _obstacles;
    
    std::vector<BonusPoints*> _bonus;
    
    std::vector<FishBubble*> _bubblesFish;
    
    std::vector<GameSprite*> _takeBonus;
};

#endif /* defined(__FlappyKitten__ObstaclesManager__) */
