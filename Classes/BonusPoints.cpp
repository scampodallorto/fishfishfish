//
//  BonusBird.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 12/02/14.
//
//

#include "BonusPoints.h"
#include "GameSprite.h"


BonusPoints::BonusPoints() {
    
    _sprite = GameSprite::create("SeaStar.png");
}

BonusPoints::~BonusPoints() {
    
    SAFE_RELEASE_OBJECT(_sprite);
}


void BonusPoints::update(float dt) {
    
    float y = _heightPath.update(dt);
    CCPoint pt = _sprite->position();
    pt.y = y;
    _sprite->position(pt);
}