//
//  Obstacles06a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles06a__
#define __FishFishFish__Obstacles06a__

#include "Obstacles01a.h"

class Obstacles06a : public Obstacles01a {
    
public:
    Obstacles06a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles06a__) */
