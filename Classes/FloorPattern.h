//
//  Pattern.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__FloorPattern__
#define __GoPenguin__FloorPattern__

#include <frw/common.h>
#include "Pattern.h"

class PatternManager;

class FloorPattern : public Pattern {
    
public:
    /**
     * ctor
     */
    FloorPattern(const char* framePattern, const char* overrideSprite = NULL);
    
    /**
     * dtor
     */
    virtual ~FloorPattern();

    /**
     * create pattern
     */
    virtual void initialize(frw::res::managerBatch* patternManager, CCPoint pt);
    
    /**
     * il pattern e' ancora nello schermo
     */
    virtual bool isOnScreen();
    
    /**
     * clona questo pattern
     */
    virtual Pattern* clone();
    
    /**
     *
     */
    virtual void move(float dx, float dy);
    
    /**
     *  torna size del pattern
     */
    virtual CCSize getSize();
    
    /**
     * ritorna risorsa cocos2dx
     */
    virtual GameSprite* getResource();
    
    /**
     * update to override
     */
    virtual void update(float dt);
   
    virtual void visible(bool v);
    
protected:
    /**
     * frame sprite
     */
    std::string _framePattern;
    std::string _framePatternOverride;
    
    GameSprite* _sprite;
    GameSprite* _spriteOverride;
};

#endif /* defined(__GoPenguin__Pattern__) */
