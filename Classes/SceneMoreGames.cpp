//
//  SceneMoreGames.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#include "SceneMoreGames.h"
#include "SceneMain.h"
#include "GameSprite.h"
#include "Sounds.h"

class ButtonGames : public frw::res::button {
    
public:
    ButtonGames() : frw::res::button("Button_MoreGames.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {
        
        frw::snd::manager::getInstance()->play(kSfx_Tap);

#if defined(__APPLE__)
        frw::platform::OpenUrl("http://www.smallthinggame.com/~promo/ios");
#else
        frw::platform::OpenUrl("http://www.smallthinggame.com/~promo/android");
#endif
    }
    
};

class ButtonSite : public frw::res::button {
    
public:
    ButtonSite() : frw::res::button("Button_Web.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {
     
        frw::snd::manager::getInstance()->play(kSfx_Tap);

        frw::platform::OpenUrl("http://www.smallthinggame.com");
    }
    
};

class ButtonFacebook : public frw::res::button {
    
public:
    ButtonFacebook() : frw::res::button("Button_FBPage.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {
        
        frw::snd::manager::getInstance()->play(kSfx_Tap);

        frw::platform::OpenUrl("https://www.facebook.com/pages/Smallthing/468268456538894?ref=hl");
    }
    
};

class ButtonExit : public frw::res::button {
    
public:
    ButtonExit() : frw::res::button("Button_Exit.png") {
        
        scale(GAMESCALE);
    }
    
    virtual void onRelease() {

        frw::snd::manager::getInstance()->play(kSfx_Tap);

        ApplicationSendMessage("MAIN", SceneMain::kMessageMoreGamesExit);
    }
    
};

SceneMoreGames::SceneMoreGames() : frw::res::menuPopup("Menu moregames") {

    _skin = GameSprite::create("Skin.png");
    _skin->priority(frw::res::overlaybase::kPriorityLow);
    _skin->position(240, 160);
    
    _buttonGames = new ButtonGames();
    _buttonSite = new ButtonSite();
    _buttonFacebook = new ButtonFacebook();
    _buttonExit = new ButtonExit();
    
    _buttonGames->position(230 - _buttonSite->size().width * GAMESCALE, 160);
    _buttonSite->position(240, 160);
    _buttonFacebook->position(250 + _buttonSite->size().width * GAMESCALE, 160);
    _buttonExit->position(_skin->position().x + _skin->size().width / 2 + (_buttonExit->size().width * GAMESCALE) * 0.1f,
                          _skin->position().y - _skin->size().height / 2 - (_buttonExit->size().height * GAMESCALE) / 2);
    
    push(_skin);
    push(_buttonGames);
    push(_buttonSite);
    push(_buttonFacebook);
    push(_buttonExit);
    
    hide(-1);
}

SceneMoreGames::~SceneMoreGames() {
    
    SAFE_RELEASE_OBJECT(_skin);
    SAFE_RELEASE_OBJECT(_buttonGames);
    SAFE_RELEASE_OBJECT(_buttonSite);
    SAFE_RELEASE_OBJECT(_buttonFacebook);
    SAFE_RELEASE_OBJECT(_buttonExit);
    
}

