//
//  Obstacles01.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#include "Obstacles01a.h"
#include "SceneMain.h"


Obstacles01a::Obstacles01a() : _down(NULL), _earned(false), _up(NULL), _speed(0), _x(0) {
    
    _type = 1;
    
    if(Obstacles::_timeToChange < 0) {
//
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    
    switch(_idObstacles) {
        case 0: {
            _up = GameSprite::create("Obstacle1.png");
            _down = GameSprite::create("Obstacle1.png");
            break;
        }
        case 1: {
            _up = GameSprite::create("Obstacle2.png");
            _down = GameSprite::create("Obstacle2.png");
            break;
        }
        case 2: {
            _up = GameSprite::create("Obstacle3.png");
            _down = GameSprite::create("Obstacle3.png");
            break;
        }
        case 3: {
            _up = GameSprite::create("Obstacle4.png");
            _down = GameSprite::create("Obstacle4.png");
            break;
        }
    }
    
    Obstacles::_timeToChange--;
    if(Obstacles::_timeToChange < 0) {
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
        _idObstacles++;
        if(_idObstacles > 3) {
            _idObstacles = 0;
        }
    }
    
    int w = _up->size().width;
    int h = _up->size().height;
    
    _x = 480 + w;
    
    float apertura = h*0.1f;
    
    switch (frw::platform::GetDeviceType()) {
        case frw::kDevice_iPad: {
            apertura = h*0.12f;
            break;
        }
            
        default: {
            apertura = h*0.12f;
            break;
        }
            
    }
    
    _up->position(_x, -apertura);
    _down->position(_x, 320+(apertura));
    
    _speed = kObstaclesSpeed;

    
}

CCPoint Obstacles01a::position() {
    
    if(_down)
        return _down->position();
    if(_up)
        return _up->position();
    return ccp(0,0);
}

Obstacles01a::~Obstacles01a() {
    
    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);
    
}

/** inserisci nella scena */
void Obstacles01a::push(frw::res::manager* manager) {
    
    manager->push(_up);
    manager->push(_down);
}

/** l'ostacolo e' stato toccato da sto sprite? */
bool Obstacles01a::isTouch(const CCRect &rect) {

    if(_up) {
        _rectUp = _up->collisionBox();
        if(_rectUp.intersectsRect(rect)) {
            return true;
        }
    }
    
    if(_down) {
        _rectDown = _down->collisionBox();
        if(_rectDown.intersectsRect(rect)) {
            return true;
        }
    }

    return false;
}

Obstacles* Obstacles01a::clone() {
    
    Obstacles* ret = new Obstacles01a();
    
    return ret;
}

void Obstacles01a::visible(bool enable) {

    SAFE_PTR(_up)->visible(enable);
    SAFE_PTR(_down)->visible(enable);
    
}

CCSize Obstacles01a::size() {
    
    if(_down) {
        return _down->size();
    }

    if(_up) {
        return _up->size();
    }
    
    return CCSize();
}

/** sposta ostacoli */
bool Obstacles01a::update(float ftime) {
    
    _x -= ftime * _speed;
    
    CCPoint pt1;
    CCPoint pt2;

    if(_up)
        pt1 = _up->position();

    if(_down)
        pt2 = _down->position();
    
    SAFE_PTR(_up)->position(_x, pt1.y);
    SAFE_PTR(_down)->position(_x, pt2.y);
    
    // ho superato l'ostacolo?
    if(_x < kFishPosition && !_earned) {
        _earned = true;
        
        ApplicationSendMessage("MAIN", SceneMain::kMessageEarnPoint);
    }
    
    float w = 0;
    if(_up) {
        w = _up->size().width;
    }
    
    if(_down) {
        if(w < _down->size().width) {
            w = _down->size().width;
        }
    }
    
    if(_up && _x < -w) {
     
        SAFE_PTR(_up)->visible(false);
        SAFE_PTR(_down)->visible(false);
        
        return true;
    }
    else if(_x < -w) {
        
        SAFE_PTR(_up)->visible(false);
        SAFE_PTR(_down)->visible(false);
        
        return true;
    }
    
    return false;
}