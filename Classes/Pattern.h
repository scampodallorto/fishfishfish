//
//  Pattern.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__Pattern__
#define __GoPenguin__Pattern__

#include <frw/common.h>

class PatternManager;
class GameSprite;

/**
 * \class Pattern
 * \brief pattern interface
 */
class Pattern {
    
public:
    /**
     * ctor
     */
    Pattern() : _manager(NULL){}
    
    /**
     * dtor
     */
    virtual ~Pattern() {}
    
    /**
     * create pattern
     */
    virtual void initialize(frw::res::managerBatch* managerBatch, CCPoint pt) = 0;

    /**
     * il pattern e' ancora nello schermo
     */
    virtual bool isOnScreen() = 0;
    
    /**
     * torna il size del pattern
     */
    virtual CCSize getSize() = 0;
    
    /**
     * clona questo pattern
     */
    virtual Pattern* clone() = 0;

    /**
     * move
     */
    virtual void move(float dx, float dy) = 0;

    /**
     * ritorna risorsa cocos2dx
     */
    virtual GameSprite* getResource() = 0;
    
    /**
     */
    virtual void visible(bool v) {}
    
    /**
     * update to override
     */
    virtual void update(float dr) {}
    
protected:
    /**
     * pattern manager
     */
    frw::res::managerBatch* _manager;
};



#endif /* defined(__GoPenguin__Pattern__) */
