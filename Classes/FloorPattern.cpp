//
//  Pattern.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#include "FloorPattern.h"
#include "Pattern.h"
#include "PatternManager.h"
#include "GameSprite.h"

/**
 * ctor
 */
FloorPattern::FloorPattern(const char* framePattern, const char* overrideSprite) : _framePattern(framePattern), _sprite(NULL), _spriteOverride(NULL) {
    
    if(overrideSprite) {
        _framePatternOverride = overrideSprite;
    }
    
}

/**
 * dtor
 */
FloorPattern::~FloorPattern() {
    
    SAFE_RELEASE_OBJECT(_sprite);
    SAFE_RELEASE_OBJECT(_spriteOverride);
}

/**
 * create pattern
 */
void FloorPattern::initialize(frw::res::managerBatch* managerBatch, CCPoint pt) {
    
    _manager = managerBatch;
    _sprite = GameSprite::create(_framePattern.c_str());
    _sprite->position(pt);
    managerBatch->push(_sprite);

    if(_framePatternOverride.length() > 0) {
        _spriteOverride = GameSprite::create(_framePatternOverride.c_str());
        _spriteOverride->priority(_sprite->priority()+1);
        _spriteOverride->position(pt);
        managerBatch->push(_spriteOverride);
    }
    
}

/**
 * il pattern e' ancora nello schermo
 */
bool FloorPattern::isOnScreen() {
    
    CCPoint pt = _sprite->position();
    CCSize s = getSize();
    int width = s.width;
    int height = s.height;
    
    CCRect rc1(0, 0, 480, 320);
    CCRect rc2(pt.x - width / 2, pt.y - height / 2, width, height);
    
    return rc1.intersectsRect(rc2);
}

/**
 * clona questo pattern
 */
Pattern* FloorPattern::clone() {
    
    FloorPattern* ret = new FloorPattern(_framePattern.c_str(), _framePatternOverride.c_str());
    ret->initialize(_manager, _sprite->position());
    
    return ret;
}

/**
 * ritorna risorsa cocos2dx
 */
GameSprite* FloorPattern::getResource() {
    
    return _sprite;
}

CCSize FloorPattern::getSize() {
    
    return _sprite->size();
}

void FloorPattern::move(float dx, float dy) {
    
    CCPoint pt = _sprite->position();
    pt.x -= dx;
    pt.y -= dy;
    _sprite->position(pt);
    SAFE_PTR(_spriteOverride)->position(pt);
}

void FloorPattern::visible(bool v) {
    
    _sprite->visible(v);
    SAFE_PTR(_spriteOverride)->visible(v);
}

void FloorPattern::update(float dt) {
    
    SAFE_PTR(_spriteOverride)->position(_sprite->position().x, _sprite->position().y - _spriteOverride->size().height*0.2f);
    
}