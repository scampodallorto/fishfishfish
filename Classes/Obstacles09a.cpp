//
//  Obstacles09a.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#include "Obstacles09a.h"

Obstacles09a::Obstacles09a() {
    
    _type = 9;
    
    if(Obstacles::_timeToChange < 0) {
        //
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    GameSprite* o = GameSprite::create("Background_Sea_1.png");
    
    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);

    _down = GameSprite::create("Mine.png");
    
    std::vector<float> args;
    CCPoint pt1;
    CCPoint pt2;
    
    pt1 = ccp(480+_down->size().width/2, 320-o->size().height/2-_down->size().height/2);
    pt2 = ccp(480+_down->size().width/2, 320-o->size().height/2-_down->size().height*1.5f);
    
    o->position(pt1);
    
    args.push_back(pt1.y);
    args.push_back(pt2.y);
    args.push_back(pt1.y);
    
    _down->pathPositionY(args, 3.0f, true);
    
    SAFE_RELEASE_OBJECT(o);
    
 
    _x = 480+_down->size().width/2;

}

/** clona questo oggetto */
Obstacles* Obstacles09a::clone() {
    
    return new Obstacles09a();
}