//
//  SceneMenu.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#ifndef __FlappyKitten__SceneMenu__
#define __FlappyKitten__SceneMenu__

#include <frw/common.h>

class Fish;

class SceneMenu : public frw::res::menuPopup, public frw::message::listener {
    
    
public:
    
    SceneMenu();
    
    virtual ~SceneMenu();
    
    DECLARATE_MESSAGE_LISTENER();
    
    enum Message {
        kMessagePlay,
        kMessageLeaderboard
    };
    
    virtual void update(float dt);
    
    /**
     * hide
     */
    virtual void hide();
    

protected:
    
    /** gattino che passa dietro */
    Fish* _fish;
    
    frw::res::sprite* _fish1Text;
    frw::res::sprite* _fish2Text;
    
    frw::res::button *_buttonStart;
    frw::res::button *_buttonLeaderboard;
    frw::res::button *_buttonMore;
    
    float _timeToFish;
    
    /** stop */
    bool _stop;
};

#endif /* defined(__FlappyKitten__SceneMenu__) */
