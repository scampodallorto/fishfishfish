//
//  Counter.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#ifndef __FlappyKitten__Counter__
#define __FlappyKitten__Counter__

#include <frw/common.h>
#include "GameSprite.h"

class Counter {
    
public:
    Counter(frw::res::manager* manager);
    virtual ~Counter();
    
    void setValue(int v);
    
    
    void position(float x, float y);
    
    inline CCPoint position() {
        return _position;
    }
    
    inline float height() {
        
        return _sizeCharacter.height;
    }
    
    void show();
    
    void hide(bool now = false);
    
private:
    
    int _value;
    float _scale;
    std::string _valueString;
    CCPoint _position;
    
    frw::res::manager* _manager;
    GameSprite* _number[10];
    CCSize _sizeCharacter;
};

#endif /* defined(__FlappyKitten__Counter__) */
