//
//  MyDirector.h
//  BubHole
//
//  Created by Sara Scarazzolo on 11/11/12.
//
//

#ifndef __BubHole__MyDirector__
#define __BubHole__MyDirector__

#include <iostream>
#include <frw/core/director.h>
#include <frw/core/scene.h>
#include <frw/file/file.h>
#include <frw/game/achievements.h>

// LOCAL
#include "MyAssets.h"
#include "MyScreen.h"

class MyDirector : public frw::core::director
{
public:
    MyDirector(const char* identifier, const frw::core::SceneDefinition* scenes);
    virtual ~MyDirector();
    
    static MyDirector* _instance;
    
protected:
    virtual void onUpdate(float elapsedTime);
    virtual void OnWillResignActive();
    virtual void OnBecomeActive();
    virtual void OnDidEnterBackground();
    virtual void OnWillEnterForeground();
    
private:
    MyAssets* _assets;
    MyScreen* _screen;
};

#endif /* defined(__BubHole__MyDirector__) */
