//
//  MyDirector.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 11/11/12.
//
//
#include <frw/common.h>
#include <frw/platform/platform.h>
#include <frw/message/message.h>
#include <frw/text/texts.h>
#include <frw/game/popupmanager.h>

#include "Sounds.h"
#include "MyDirector.h"

/*
#if defined(ANDROID)

//--------------------------------------------------------------------------------------------------------------------------------------------

const frw::game::AchievementInfo achievementsInfo[] =
{
    { -1, NULL}
};

#else

const frw::game::AchievementInfo achievementsInfo[] =
{
    { -1, NULL}
};

#endif

*/

#if defined(ENABLE_LOCAL_NOTIFICATION)
void onNotificationCallback() {
    
}

std::string _localNotificationIta = "Nuota con i tre pesciolini e supera gli ostacoli!";
std::string _localNotificationEng = "Swims with three fish and overcomes obstacles!";
int _locatNotificationTimeInSeconds = 60*60*24*3;
#endif

MyDirector* MyDirector::_instance = NULL;;


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#define fileSound(x) "/"x

#else

#define fileSound(x) x

#endif


frw::snd::soundset soundSet[] = {

    frw::snd::soundset(kMusic_Game, "sounds/Music.mp3", frw::snd::kMusic, true),    
    frw::snd::soundset(kSfx_Tap, "sounds/MenuButton.mp3", frw::snd::kSfx, false),
    frw::snd::soundset(kSfx_Point, "sounds/Point.mp3", frw::snd::kSfx, false),
    frw::snd::soundset(kSfx_Dead, "sounds/Sbatte.mp3", frw::snd::kSfx, false),
    frw::snd::soundset(kSfx_Fall, "sounds/Fall.mp3", frw::snd::kSfx, false),
    frw::snd::soundset(kSfx_TakeBonus, "sounds/StarTake.mp3", frw::snd::kSfx, false),
    frw::snd::soundset(kSfx_Jump, "sounds/JumpTap.mp3", frw::snd::kSfx, false),

};


MyDirector::MyDirector(const char* identifier, const frw::core::SceneDefinition* scenes) : frw::core::director(identifier, scenes)
{
    frw::file::manager::initialize();

    _INFO("LOAD loadSoundSets");
    frw::snd::manager::getInstance()->loadSoundSets(soundSet, sizeof(soundSet)/sizeof(frw::snd::soundset));
    _INFO("LOADED loadSoundSets");
    
    _INFO("LOAD MyScreen");
    _screen = new MyScreen();
    _INFO("LOADED MyScreen");

    _INFO("LOAD MyAssets");
    _assets = new MyAssets();
    _INFO("LOADED MyAssets");

    _instance = this;
    
    frw::text::manager::getInstance()->load("common/texts.xml");
    frw::text::manager::getInstance()->overrideLanguage("common/ru.txt", kLanguageRussian);
    
    //@ alloca facebook
    // frw::platform::SocialSystem_FacebookAlloc();
    
    frw::platform::SocialSystem_Connect();
    
    // internet connection
    frw::platform::System_CheckInternetConnectionRun();
    
    // baloon manager
    frw::game::popupmanager::initialize();
    
    frw::platform::SystemiAD_init();
    frw::platform::SystemiAD_show();
    
    // carica chartboost
    frw::platform::SystemChartboost_init();
    
    frw::res::manager::addSpriteDataSheet("resources_game.plist");
}

MyDirector::~MyDirector()
{
    frw::res::manager::removeSpritesFromDataSheet("resources_game.plist");
    

    frw::game::popupmanager::finalize();
    
    // frw::platform::SocialSystem_FacebookDealloc();
    
    frw::file::manager::finalize();
    
    _instance = NULL;
    
    delete _screen;
    _screen = NULL;
    
    delete _assets;
    _assets = NULL;
}

void MyDirector::OnWillResignActive()
{
    _INFO("MyDirector::OnWillResignActive()");
}

void MyDirector::OnBecomeActive()
{
    _INFO("MyDirector::OnBecomeActive()");
}

void MyDirector::OnDidEnterBackground()
{
    _INFO("MyDirector::OnDidEnterBackground()");
}

void MyDirector::OnWillEnterForeground()
{
    _INFO("MyDirector::OnWillEnterForeground()");    
}

void MyDirector::onUpdate(float elapsedTime)
{
    /// chec autorelease
    frw::snd::manager::getInstance()->update();
    frw::message::manager::getInstance()->onUpdate(elapsedTime);
    frw::game::popupmanager::getInstance()->update(elapsedTime);
}



