//
//  MyAssets.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#include "MyAssets.h"
#include <frw/core/director.h>
#include <frw/file/file.h>
#include <frw/platform/platform.h>
#include <frw/dsp/screen.h>
#include <frw/common.h>

MyAssets::MyAssets()
{
    float _iphoneretio = 480.0f / 320.0f;
    float r = 0;
    if(frw::dsp::screen::getInstance()->height() > frw::dsp::screen::getInstance()->width()) {
        r = ((float)frw::dsp::screen::getInstance()->height()) / (float)frw::dsp::screen::getInstance()->width();
    }
    else {
        r = ((float)frw::dsp::screen::getInstance()->width()) / (float)frw::dsp::screen::getInstance()->height();
    }
    
    if(r >= _iphoneretio)
    {
        _INFO(" SET IPHONE RESOURCES %f >= %f", r, _iphoneretio);
        frw::file::manager::getInstance()->setResourceDirectory("iphone");
    }
    else if(r < _iphoneretio)
    {
        _INFO(" SET IPAD RESOURCES %f < %f", r, _iphoneretio);
        frw::file::manager::getInstance()->setResourceDirectory("ipad");
    }
    else
    {
        _WARNING(" RISORSE NON SETTATE (%dx%d)", frw::dsp::screen::getInstance()->width(), frw::dsp::screen::getInstance()->height());
    }
    
    _INFO(" RESOURCES PATH: %s", frw::file::manager::getInstance()->getResourceDirectory().c_str());
}

