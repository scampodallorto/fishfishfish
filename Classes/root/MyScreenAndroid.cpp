//
//  MyScreen.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#include "MyScreen.h"
#include <frw/platform/platform.h>
#include <frw/common.h>

MyScreen::MyScreen()
{
    _INFO(" ANDROID (%dx%d)", width(), height());
    float w = 480.0f;
    float h = 320.0f;
    
    float _iphoneretio = w / h;
    
    _ratio.x = width() / w;
    _ratio.y = ((h/w)*width()) / h;
    
    _ratio_content = _ratio;
    
    float r = 0;
    if(frw::dsp::screen::getInstance()->height() > frw::dsp::screen::getInstance()->width()) {
        r = ((float)frw::dsp::screen::getInstance()->height()) / (float)frw::dsp::screen::getInstance()->width();
    }
    else {
        r = ((float)frw::dsp::screen::getInstance()->width()) / (float)frw::dsp::screen::getInstance()->height();
    }
    
    if(r >= _iphoneretio)
    {
        _ratio.x = width() / w;
        _ratio.y = height() / h;
        
        CCDirector::sharedDirector()->setContentScaleFactor(768.0f/(float)height());
        
        _black_band.x = 0;
        _black_band.y = 0; //(height() - (h/w)*width()) / 2;
    }
    else if(r < _iphoneretio)
    {
        _ratio.x = width() / w;
        _ratio.y = height() / h;
        
        CCDirector::sharedDirector()->setContentScaleFactor(1024.0f/(float)width());
        
        _black_band.x = 0;
        _black_band.y = 0;
    }
    else
    {
        _WARNING(" RISORSE NON SETTATE (%dx%d)", frw::dsp::screen::getInstance()->width(), frw::dsp::screen::getInstance()->height());
    }
    
    _ratio_content = _ratio;
    
    _INFO(" RATIO: (%fx%f)", _ratio.x, _ratio.y);

}

cocos2d::ccVertex2F MyScreen::getRatioPosition()
{
    return _ratio;
}

cocos2d::ccVertex2F MyScreen::getRatioContent()
{
    return _ratio_content;
}
