//
//  BubbleBox2AppDelegate.cpp
//  BubbleBox2
//
//  Created by Sara Scarazzolo on 29/11/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#include "AppDelegate.h"

#include "cocos2d.h"

#include "MyDirector.h"
#include "SceneLoading.h"
#include "SceneMain.h"
#include "SceneFakeToMain.h"

USING_NS_CC;

const frw::core::SceneDefinition _scenes[] = {

    "SceneLoading", INSERT_SCENE(SceneLoading),
    "SceneMain", INSERT_SCENE(SceneMain),
    "SceneFakeToMain", INSERT_SCENE(SceneFakeToMain),
    
    kNullScene
};

USING_NS_CC;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

    // turn on display FPS
    //pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    // pDirector->setAnimationInterval(1.0 / 60);

    // LEEN: CREATE MY DIRECTOR
    
    MyDirector::create(new MyDirector("MY DIRECTORY", _scenes));
#if defined(DEBUG)
    frw::core::director::getInstance()->setNextScene("SceneLoading");
//        frw::core::director::getInstance()->setNextScene("SceneIntro");
//    frw::core::director::getInstance()->setNextScene("SceneMain");
    //frw::core::director::getInstance()->setNextScene("SceneFakeToMain");
#else
    frw::core::director::getInstance()->setNextScene("SceneLoading");
#endif
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    MyDirector::_instance->Pause();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    MyDirector::_instance->Resume();
}






