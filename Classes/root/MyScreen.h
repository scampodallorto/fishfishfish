//
//  MyScreen.h
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#ifndef __BubHole__MyScreen__
#define __BubHole__MyScreen__

#include <iostream>
#include <frw/dsp/screen.h>

class MyScreen : public frw::dsp::screen
{
public:
    MyScreen();
    
    virtual cocos2d::ccVertex2F getRatioPosition();
    virtual cocos2d::ccVertex2F getRatioContent();

    
private:

    cocos2d::ccVertex2F _ratio;
    cocos2d::ccVertex2F _ratio_content;
};


#endif /* defined(__BubHole__MyScreen__) */
