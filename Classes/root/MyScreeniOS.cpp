//
//  MyScreen.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#include "MyScreen.h"
#include <frw/platform/platform.h>
#include <frw/common.h>

MyScreen::MyScreen()
{
//#define GAME_LANDSCAPE
#if defined(GAME_LANDSCAPE)
    
    /*
     2048: 224
     1024: 112
     480: 52
     960: 105
     
     (480/1024)*x
     
     
     */
    float w = 480.0f;
    float h = 320.0f;
    
    _ratio.x = width() / w;
    _ratio.y = ((h/w)*width()) / h;
    
    _ratio_content = _ratio;

    switch (frw::platform::GetDeviceType()) {
            
        case frw::kDevice_iPad:
        {
            _ratio.x = width() / w;
            _ratio.y = height() / h;
            
            CCDirector::sharedDirector()->setContentScaleFactor(1024.0f/(float)width());
            break;
        }
        case frw::kDevice_iPad3:
        {
            _ratio.x = width() / w;
            _ratio.y = height() / h;
            
            CCDirector::sharedDirector()->setContentScaleFactor(1024.0f/(float)width());
            break;
        }
            
        case frw::kDevice_iPhone:
        {
            CCDirector::sharedDirector()->setContentScaleFactor(768.0f/(float)height());
            break;
        }
        case frw::kDevice_iPhone4:
        {
            CCDirector::sharedDirector()->setContentScaleFactor(768.0f/(float)height());
            
            _black_band.x = 0;
            _black_band.y = 0;
            
            break;
        }
            
        case frw::kDevice_iPhone5:
        {
            _ratio.x = width() / w;
            _ratio.y = height() / h;
            
            CCDirector::sharedDirector()->setContentScaleFactor(768.0f/(float)height());
            
            _black_band.x = 0;
            _black_band.y = 0; //(height() - (h/w)*width()) / 2;
            
            break;
        }
            
        default:
            break;
    }
    
#else
    // SI LAVORA IN 320x480, quindi cerco la proporzione sui 320x480 con la risoluzione
    
    _INFO(" iOS (%dx%d)", width(), height());
    
    _ratio.x = width() / 320.0f;
    _ratio.y = ((480.0f/320.0f)*width()) / 480.0f; // height() / 480.0f;
    
    _ratio_content = _ratio;
    
    switch (frw::platform::GetDeviceType()) {
            
        case frw::kDevice_iPad:
        {
            _ratio.x = width() / 320.0f;
            _ratio.y = height() / 480.0f;
            
            CCDirector::sharedDirector()->setContentScaleFactor(1024.0f/(float)height());
            break;
        }
        case frw::kDevice_iPad3:
        {
            _ratio.x = width() / 320.0f;
            _ratio.y = height() / 480.0f;
            
            //CCDirector::sharedDirector()->enableRetinaDisplay(true);
            CCDirector::sharedDirector()->setContentScaleFactor(2048.0f/(float)height());
            break;
        }
            
        case frw::kDevice_iPhone:
        {
            CCDirector::sharedDirector()->setContentScaleFactor(960.0f/(float)height());
            break;
        }
        case frw::kDevice_iPhone4:
        {
            CCDirector::sharedDirector()->setContentScaleFactor(1.0f);
            
            _black_band.x = 0;
            _black_band.y = 0; // (height() - (480.0f/320.0f)*width()) / 2;
            
            break;
        }
            
        case frw::kDevice_iPhone5:
        {
            /*
             CCDirector::sharedDirector()->setContentScaleFactor(1.0f);
             
             _black_band.x = 0;
             _black_band.y = 0;//(height() - (480.0f/320.0f)*width()) / 2;
             
             _ratio.x = width() / 320.0f;
             _ratio.y = height()/ 480.0f; // ((480.0f/320.0f)*width()) / 480.0f; // height() / 480.0f;
             */
            _ratio.x = width() / 320.0f;
            _ratio.y = ((480.0f/320.0f)*width()) / 480.0f;
            
            CCDirector::sharedDirector()->setContentScaleFactor(640.0f/(float)width());
            
            _black_band.x = 0;
            _black_band.y = (height() - (480.0f/320.0f)*width()) / 2;
            
            break;
        }
            
        default:
            break;
    }
#endif
}

cocos2d::ccVertex2F MyScreen::getRatioPosition()
{
    return _ratio;
}

cocos2d::ccVertex2F MyScreen::getRatioContent()
{
    return _ratio_content;
}


