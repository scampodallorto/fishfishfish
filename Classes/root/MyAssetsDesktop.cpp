//
//  MyAssets.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#include "MyAssets.h"
#include <frw/core/director.h>
#include <frw/file/file.h>
#include <frw/platform/platform.h>
#include <frw/dsp/screen.h>
#include <frw/common.h>

MyAssets::MyAssets()
{
    frw::file::manager::getInstance()->setResourceDirectory("mac");
}

