//
//  MyAssets.h
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#ifndef __BubHole__MyAssets__
#define __BubHole__MyAssets__

#include <iostream>
#include <frw/assets/assets.h>

class MyAssets : public frw::assets::manager
{
public:
    MyAssets();
};

#endif /* defined(__BubHole__MyAssets__) */
