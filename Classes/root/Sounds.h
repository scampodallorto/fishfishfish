#pragma once

#include <frw/snd/sound.h>


enum  {
    
    kMusicID = -2,
    kSfxID,

    kMusic_Game,
    
    kSfx_Tap,
    kSfx_Point,
    kSfx_Dead,
    kSfx_Fall,
    kSfx_TakeBonus,
    kSfx_Jump,
    
    kSoundCount
};