//
//  MyScreen.cpp
//  BubHole
//
//  Created by Sara Scarazzolo on 12/11/12.
//
//

#include "MyScreen.h"
#include <frw/platform/platform.h>
#include <frw/common.h>

MyScreen::MyScreen()
{
    float w = 480;
    float h = 320;
    
    _ratio.x = width() / w;
    _ratio.y = ((h/w)*width()) / h;
    
    _ratio_content = _ratio;
    
    CCDirector::sharedDirector()->setContentScaleFactor(960/(float)width());
}

cocos2d::ccVertex2F MyScreen::getRatioPosition()
{
    return _ratio;
}

cocos2d::ccVertex2F MyScreen::getRatioContent()
{
    return _ratio_content;
}

cocos2d::ccVertex2F MyScreen::getWorksResolution() {
    
    return {480, 320};
}
