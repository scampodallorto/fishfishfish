//
//  Obstacles13a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles13a__
#define __FishFishFish__Obstacles13a__

#include "Obstacles01a.h"

class Obstacles13a : public Obstacles01a {
    
public:
    Obstacles13a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles13a__) */
