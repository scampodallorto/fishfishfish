//
//  SceneGameOver.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 11/02/14.
//
//

#ifndef __FlappyKitten__SceneGameOver__
#define __FlappyKitten__SceneGameOver__

#include <frw/common.h>

class Counter;
class GameSprite;

class SceneGameOver : public frw::res::menuPopup {
    
public:
    SceneGameOver(GameSprite* floor);
    virtual ~SceneGameOver();

    void show(int score);
    
    virtual void update(float dt);
    
    inline bool isEnd() {
        return _state == kEnd;
    }
    
    frw::res::button* buttonRating() {
        return _buttonRating;
    }
    
private:
    
    enum State {
        kIdle,
        kWaitShow,
        kCountPoint,
        kEnd
    };
    
    State _state;
    
    float _timeToScore;
    float _delayTime;
    int _score;
    int _counterPoints;
    int _heightSkinOnScreen;
    int _currentBestScore;
    
    frw::res::button* _buttonRating;
    
    GameSprite* _skin;
    GameSprite* _game;
    GameSprite* _over;
    Counter* _currentScore;
    Counter* _bestScore;
};

#endif /* defined(__FlappyKitten__SceneGameOver__) */
