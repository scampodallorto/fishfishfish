//
//  SceneMain.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#include "SceneMain.h"
#include "FloorPattern.h"
#include "SeaPattern.h"
#include "GameSprite.h"
#include "Fish.h"
#include "FishRed.h"
#include "FishYellow.h"
#include "FishViolet.h"
#include "Counter.h"
#include "SceneMenu.h"
#include "SceneMoreGames.h"
#include "Sounds.h"
#include "SceneGameOver.h"
#include "Obstacles.h"

CREATE_SCENE(SceneMain)

#define kTimeToFishes (frw::math::random::Range(2.0f,5.0f))

/**
 * ctor
 */
SceneMain::SceneMain(const char* identifier) : frw::core::scene(identifier), _patterManagerFloor(NULL), _patterManagerSky(NULL), _backgroundColor(NULL), _batchRender(NULL), _layerBackgroundColor(NULL), _layerPatterns(NULL), _state(kIdle), _alphaNewSkyPattern(frw::math::PathAlpha()), _currentWorld(0), _currentPoint(0), _bestScore(0), _fish(NULL), _counter(NULL), _menu(NULL), _moreGames(NULL), _sceneGameOver(NULL), _finger(NULL), _obstaclesManager(NULL), _timeToStart(NULL), _counterFishTime(0), _iDFish(0), _timeToFish(0) {
    
    _bestScore = CCUserDefault::sharedUserDefault()->getIntegerForKey(kKeyBestScore);
    
#if defined(__APPLE__)
    frw::platform::SocialSystem_LeaderboardReportScore(_bestScore, "com.smallthinggame.tinyfish.records2");
#else
    frw::platform::SocialSystem_LeaderboardReportScore(_bestScore, "13636");
#endif
}

/**
 * dtor
 */
SceneMain::~SceneMain() {
    
}

/**
 * override to initialize the scene's objects
 */
void SceneMain::initialize() {
    
    RegisterMessageListener("MAIN");
    
    // carica mondo
    WorldPatter::loadFromXML("common/config.xml");
    
    memset(_worldsUsed, 0, sizeof(_worldsUsed));
    _worldsUsed[0] = true; // il mondo 0 e' da dove partiamo
    _currentWorld = 0;
    
    _layerBackgroundColor = frw::res::manager::create("Layer background color");
    _backgroundColor = frw::res::quad::create("background color");
    _backgroundColor->color(0x4b, 0xb4, 0xea);
    _backgroundColor->position(240, 160);
    _backgroundColor->disableBlackBand();
    _layerBackgroundColor->push(_backgroundColor);
    push(_layerBackgroundColor, 0);
    
    
    _layerPatterns = frw::res::manager::create("Layer batch render");
    _batchRender = frw::res::managerBatch::create("resources_game.png");
    _layerPatternsWave = frw::res::manager::create("Layer batch render wave");
    _batchRenderWave = frw::res::managerBatch::create("resources_game.png");
    _layerPatterns->push(_batchRender);
    _layerPatternsWave->push(_batchRenderWave);

    // floor patterns
    _patterManagerFloor = PatternHScrollManager::create(kObstaclesSpeed, _batchRender, new SeaPattern("Background_Sea_1.png"));
    _patterManagerFloor->addPattern(new SeaPattern("Background_Sea_1.png", "Background_Sea_2.png"));
    _patterManagerFloor->addPattern(new SeaPattern("Background_Sea_1.png", "Background_Sea_3.png"));
    _patterManagerFloor->addPattern(new SeaPattern("Background_Sea_1.png", "Background_Sea_4.png"));
    
    _patterManagerFloor->setPriority(frw::res::overlaybase::kPriorityNormal);
    ((PatternHScrollManager*)_patterManagerFloor)->addListener(this);

    // sky patterns
    // per posizionare il cielo, bisogna calcolare la posizone sopra il floor
    int hFloor = _patterManagerFloor->getFirstPattern()->getSize().height;
    frw::res::sprite* bg = frw::res::manager::createSpriteByFrame("ScenarySea_a.png");
    int hBackground = bg->size().height*GAMESCALE;
    Pattern* firstSky = *_patterManagerFloor->getListPatterns().begin();
    CCPoint pt = firstSky->getResource()->position();
    pt.y -= hBackground/2;
    pt.y -= hFloor/2;
    _patterManagerSky = PatternHScrollManager::create(20, _batchRender, new FloorPattern("ScenarySea_a.png"), pt.y);
    _patterManagerSky->setPriority(frw::res::overlaybase::kPriorityLow);
    _patterManagerSky->addPattern(new FloorPattern("ScenarySea_b.png"));
    _patterManagerSky->addPattern(new FloorPattern("ScenarySea_c.png"));
    _patterManagerSky->addPattern(new FloorPattern("ScenarySea_d.png"));
    
    _patterManagerWave = PatternHScrollManager::create(40, _batchRenderWave, new FloorPattern("Wave.png"), pt.y);
    _patterManagerWave->setPriority(frw::res::overlaybase::kPriorityHigh);
    
    push(_layerPatterns, 10);
    
    // obstacles manager
    _obstaclesManager = new ObstaclesManager();
    push(_obstaclesManager, 11);

    push(_layerPatternsWave, 12);

    // gattino
    _fishSource[0] = FishRed::create(_obstaclesManager);
    _fishSource[1] = FishYellow::create(_obstaclesManager);
    _fishSource[1]->visibility(false);
    _fishSource[2] = FishViolet::create(_obstaclesManager);
    _fishSource[2]->visibility(false);
    
    _counter = new Counter(_layerPatternsWave);
    _counter->setValue(0);
    _counter->position(240, _counter->height()*1.2f);
    _counter->hide(true);
    
    // layer menu
    _menu = new SceneMenu();
    push(_menu, 20);
    
    // layer more games
    _moreGames = new SceneMoreGames();
    push(_moreGames, 21);
    
    _finger = GameSprite::create("Tap_Frame1.png");
    _finger->addAnimationFromPlist("start", "tap.plist");
    _finger->playAnimation("start", true);
    _finger->visible(false);
    _obstaclesManager->push(_finger);
    
    frw::callback::manager::TouchBegan() += this;
    
    _sceneGameOver = new SceneGameOver( _patterManagerFloor->getFirstPattern()->getResource() );
    
    _bolla = GameSprite::create("Bubble_Char.png");
    _bolla->position(ccp(-9999,-9999));
    _bolla->priority(frw::res::overlaybase::kPriorityNormal+1);
    _obstaclesManager->push(_bolla);

    _bollaExplosion = GameSprite::create("Bubble Explosion.png");
    _bollaExplosion->position(ccp(-9999,-9999));
    _bollaExplosion->priority(frw::res::overlaybase::kPriorityNormal+2);
    _obstaclesManager->push(_bollaExplosion);
    
    _bollaExplosion->visible(false);
    
    _flashLayer = frw::res::manager::create("flash later");
    _flashDead = frw::res::quad::create("flash");
    _flashDead->disableBlackBand();
    _flashDead->position(240, 160);
    _flashDead->color(255, 255, 255);
    _flashLayer->push(_flashDead);
    push(_flashLayer, 30);

    _flashDead->alpha(0);
}

/**
 * override to finalize and delete the scene's objects
 */
void SceneMain::finalize() {
    
    UnRegisterMessageListener();
    
    
    for(int i = 0; i< _fishes.size(); ++i) {
        
        GameSprite* o = _fishes[i];
        SAFE_RELEASE_OBJECT(o);
    }
    
    ((PatternHScrollManager*)_patterManagerFloor)->removeListener(this);

    SAFE_DELETE(_patterManagerSky);
    SAFE_DELETE(_patterManagerFloor);
    SAFE_DELETE(_patterManagerWave);
    SAFE_DELETE(_fishSource[0]);
    SAFE_DELETE(_fishSource[1]);
    SAFE_DELETE(_fishSource[2]);
    SAFE_DELETE(_counter);
    SAFE_RELEASE_OBJECT(_bolla);
    SAFE_RELEASE_OBJECT(_bollaExplosion);
    SAFE_RELEASE_OBJECT(_flashDead);
    
    _fish = NULL;
    
    SAFE_RELEASE_OBJECT(_batchRender);
    SAFE_RELEASE_OBJECT(_batchRenderWave);
    
    SAFE_RELEASE_MANAGER(_flashLayer);
    SAFE_RELEASE_MANAGER(_layerPatterns);
    SAFE_RELEASE_MANAGER(_layerPatternsWave);
    SAFE_RELEASE_MANAGER(_layerBackgroundColor);
    SAFE_RELEASE_MANAGER(_obstaclesManager);
    SAFE_RELEASE_MANAGER(_menu);
    SAFE_RELEASE_MANAGER(_moreGames);
    SAFE_RELEASE_MANAGER(_sceneGameOver);

}

/**
 * transiton finished
 * \see frw::core::director::setNextScene
 */
void SceneMain::transitionFinish() {
    
//    playGame();
    if(!_userdata)
        frw::platform::SystemChartboost_showInterestitial();
}

void SceneMain::playGame() {
    
    // TODO: test

    _state = kPrepareGame;
}

void SceneMain::onPatternsDistance(float &distance) {
    
}

void SceneMain::onPatternsStopped() {
    
}

void SceneMain::onFinishedPatterns() {
    
}

CREATE_CALLBACK_LISTENER(SceneMain) {
    
    if(frw::callback::manager::TouchBegan() == manager && (_state >= kKittenWaitTouch && _state < kOhOh) && !_fishSource[0]->isPathPosition()) {
        
        if(_bolla) {
            _fishSource[0]->visibility(false);
            _fishSource[1]->visibility(false);
            _fishSource[2]->visibility(false);
            _fishSource[_iDFish]->visibility(true);
            
            std::vector<float> argsS;
            argsS.push_back(1.0f);
            argsS.push_back(1.5f);
            
            std::vector<float> argsA;
            argsA.push_back(1.0f);
            argsA.push_back(.0f);
            
            _bollaExplosion->position(_bolla->position());
            _bollaExplosion->visible(true);
            _bollaExplosion->pathAlpha(argsA, 1.0f);
            _bollaExplosion->pathScale(argsS, 1.0f);
            
            SAFE_RELEASE_OBJECT(_bolla);
            
            _fish = _fishSource[_iDFish];
        }
        
        _fish->up();
        
        if(_state == kKittenWaitTouch) {
            
            frw::snd::manager::getInstance()->play(kSfx_Tap);
            
            _state = kPrepareGame;
        }
        
        _finger->visible(false);
    }
    
    if(frw::callback::manager::TouchBegan() == manager && _state == kResult && _sceneGameOver->isEnd()) {
        
        if(_sceneGameOver->buttonRating()->touch(manager->fData[0], manager->fData[1]))
            return;
        
        frw::core::director::getInstance()->setNextScene("SceneFakeToMain", frw::core::director::kTransitionProgressRadialCW, 1.0f, this);

        _state = kWaitReStart;
        
        frw::snd::manager::getInstance()->stopMusic();
        
        frw::snd::manager::getInstance()->play(kSfx_Tap);
    }
    
}

CREATE_MESSAGE_LISTENER(SceneMain) {
    
    switch (uiMessage) {
            
        case kMessageMoreGames: {
            
            _menu->disable();
            _moreGames->show();
            
            break;
        }
            
        case kMessageMoreGamesExit: {
            
            _menu->enable();
            _moreGames->hide();
            
            break;
        }
            
        case kMessagePlay: {
            
            _menu->disable();
            _menu->hide();
            
            _state = kIntroKitten;
            
            // fai entrare il fatto
            std::vector<CCPoint> args;
            args.push_back(ccp(-_fishSource[0]->size().width,160));
            args.push_back(ccp(kFishPosition,160));
            
            for(int i = 0; i < 3; ++i) {
                _fishSource[i]->pathPosition(args, 3.0f);
            }
            _bolla->pathPosition(args, 3.0f);
            _finger->position(kFishPosition + _fishSource[0]->size().width*2, 160);
            
            break;
        }
         
        case kMessageEarnPoint: {
            
            _currentPoint++;
            
            frw::snd::manager::getInstance()->play(kSfx_Point);
            
            _counter->setValue(_currentPoint);
            
            break;
        }
            
        case kMessageDead: {
            
            ((PatternHScrollManager*)_patterManagerFloor)->stopScroll();
            ((PatternHScrollManager*)_patterManagerSky)->stopScroll();

            _obstaclesManager->stop();
            
            _fish->dead();
            
            _state = kOhOh;
            
            std::vector<float> args;
            args.push_back(1.0f);
            args.push_back(.0f);
            
            _flashDead->pathAlpha(args, 0.3);

            break;
        }
            
        default:
            break;
    }
    
}

/**
 * update scene's objecys
 * \param dt delta time in ms
 */
void SceneMain::update(float dt) {


    SAFE_PTR(_patterManagerSky)->update(dt);
    SAFE_PTR(_patterManagerFloor)->update(dt);
    SAFE_PTR(_patterManagerWave)->update(dt);
    
    if(_fish) {
        SAFE_PTR(_obstaclesManager)->update(_fish, dt);
    }
    else {
        SAFE_PTR(_obstaclesManager)->update(_fishSource[0], dt);
    }
    
    SAFE_PTR(_menu)->update(dt);
    SAFE_PTR(_moreGames)->update(dt);
    
    SAFE_PTR(_fish)->update(dt);
    SAFE_PTR(_sceneGameOver)->update(dt);

    SAFE_PTR(_flashLayer)->update(dt);
    
    _timeToFish -= dt;
    if(_timeToFish < 0) {
        _timeToFish = kTimeToFishes;
        
        GameSprite* o;
        if(frw::math::random::Range(0, 100) < 50) {
            o = GameSprite::create("Fish1.png");
        }
        else {
            o = GameSprite::create("Fish2.png");
        }

        
        CCPoint pt1;
        CCPoint pt2;
        
        if(frw::math::random::Range(0, 100) < 50) {
            
            pt1.x = -o->size().width;
            pt2.x = 480+o->size().width;
            o->flipx(true);
        }
        else {

            pt1.x = 480+o->size().width;
            pt2.x = -o->size().width;
        }
        
        float t = frw::math::random::Range(5, 10);
        
        if(frw::math::random::Range(0, 100) < 50) {
            
            o->scale(frw::math::random::Range(0.3f, 0.7f));
            o->color(128, 128, 255);
            t+=5;
        }
        
        pt1.y = frw::math::random::Range(100+o->size().height, 300-o->size().height);
        pt2.y = pt1.y;

        std::vector<float> argsY;
        argsY.push_back(pt1.y);
        argsY.push_back(pt1.y + 10.0f*o->scale());
        argsY.push_back(pt1.y);
        
        std::vector<float> argsX;
        argsX.push_back(pt1.x);
        argsX.push_back(pt2.x);
        
        
        o->position(pt1);
        o->pathPositionX(argsX, t);
        o->pathPositionY(argsY, 2.0f, true);
        
        o->alpha(0.7f*o->scale());
        
        _obstaclesManager->push(o);
        
        _fishes.push_back(o);
    }
    
    
    for(int i = _fishes.size()-1; i >= 0; --i) {

        GameSprite* o = _fishes[i];
        if(!o->isPathPositionX()) {
            SAFE_RELEASE_OBJECT(o);
            _fishes.erase(_fishes.begin()+i);
        }
    }
    
    switch (_state) {
        case kKittenWaitTouch:
        case kIntroKitten: {
            
            if(_menu && !_menu->isVisible()) {
                SAFE_RELEASE_MANAGER(_menu);
            }
            
            _counterFishTime += dt;
            if(_counterFishTime > 0.2f) {
                _iDFish++;
                _counterFishTime = 0;
                if(_iDFish > 2) {
                    _iDFish = 0;
                }
                
                _fishSource[0]->visibility(false);
                _fishSource[1]->visibility(false);
                _fishSource[2]->visibility(false);
                _fishSource[_iDFish]->visibility(true);
            }
            
            break;
        }
        default:
            break;
    }
    
    switch (_state) {
        case kIdle: {
            
            break;
        }
        
        case kIntroKitten: {
            
            if(!_fishSource[0]->isPathPosition()) {
                
                _state = kKittenWaitTouch;
                _finger->visible(true);
            }
            
            break;
        }
            
        case kKittenWaitTouch: {
            
            break;
        }
            
        case kPrepareGame: {
            
            frw::snd::manager::getInstance()->play(kMusic_Game);
            
            _state = kWaitObstacles;
            
            _timeToStart = 1.0f;
            
            break;
        }
            
        case kWaitObstacles: {

            _timeToStart -= dt;
            if(_timeToStart < 0) {
                _obstaclesManager->start();
                _state = kGame;
            }
            break;
        }
            

        case kGame: {

            int hFloor = _patterManagerFloor->getFirstPattern()->getSize().height;
            
            CCPoint ptFish;
            ptFish = _fish->sprite()->position();
            if(ptFish.y > 320 - hFloor - _fish->size().height/2) {
                
                ((PatternHScrollManager*)_patterManagerFloor)->stopScroll();
                ((PatternHScrollManager*)_patterManagerSky)->stopScroll();
                
                _obstaclesManager->stop();
                
                _fish->dead();
                
                std::vector<float> args;
                args.push_back(1.0f);
                args.push_back(.0f);
                
                _flashDead->pathAlpha(args, 0.3);
                
                _state = kOhOh;
            }
            
            
            break;
        }
            
        case kOhOh: {
            
            if(_fish->isDead()) {
                
                _obstaclesManager->clearObstacles();
                
                _state = kResult;
                
                _counter->hide(true);
                
                _sceneGameOver->show(_currentPoint);
                
                push(_sceneGameOver, 25);
                
                if(_currentPoint > _bestScore) {
                    CCUserDefault::sharedUserDefault()->setIntegerForKey(kKeyBestScore, _currentPoint);
                    
#if defined(__APPLE__)
                    frw::platform::SocialSystem_LeaderboardReportScore(_currentPoint, "com.smallthinggame.tinyfish.records2");
#else
                    frw::platform::SocialSystem_LeaderboardReportScore(_currentPoint, "13636");
#endif
                }
                
            }
            
            break;
        }
            
        case kResult: {
            
            break;
        }

            
        default:
            break;
    }
    
}



