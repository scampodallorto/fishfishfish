//
//  Obstacles13a.cpp
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#include "Obstacles13a.h"

Obstacles13a::Obstacles13a() {
    
    _type = 13;
    
    if(Obstacles::_timeToChange < 0) {
        //
        Obstacles::_timeToChange = frw::math::random::Range(10.0f, 20.0f);
    }
    
    GameSprite* o = GameSprite::create("Background_Sea_1.png");
    
    SAFE_RELEASE_OBJECT(_up);
    SAFE_RELEASE_OBJECT(_down);

    _up = GameSprite::create("Obstacle_Mid.png");
    _down = GameSprite::create("Obstacle1_Hor.png");
    
    _up->position(480+_down->size().width/2, o->size().height*1.65f);
    _down->position(480+_down->size().width/2, 320-o->size().height*2);
    
    SAFE_RELEASE_OBJECT(o);
    

    _x = 480+_down->size().width/2;
}

/** clona questo oggetto */
Obstacles* Obstacles13a::clone() {
    
    return new Obstacles13a();
}