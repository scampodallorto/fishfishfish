//
//  Obstacles05.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#include "Obstacles05a.h"

Obstacles05a::Obstacles05a() {
    
    _type = 5;

    CCPoint pt1 = _up->position();
    CCPoint pt2 = _down->position();
    
    int h = _up->size().height * 0.15f;
    
    pt1.y += h*1.2f;
    pt2.y += h*1.2f;
    
    _up->position(pt1);
    _down->position(pt2);
}

/** clona questo oggetto */
Obstacles* Obstacles05a::clone() {
    
    return new Obstacles05a();
}