//
//  Obstacles15a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 15/02/14.
//
//

#ifndef __FishFishFish__Obstacles15a__
#define __FishFishFish__Obstacles15a__

#include "Obstacles01a.h"

class Obstacles15a : public Obstacles01a {
    
public:
    Obstacles15a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles15a__) */
