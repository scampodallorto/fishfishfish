//
//  GameSprite.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#include "GameSprite.h"

GameSprite::GameSprite(const char* identifier) : frw::res::sprite(identifier) {
    
}

GameSprite::~GameSprite() {

}

GameSprite* GameSprite::create(const char* frame) {
    
    CCSpriteFrame* o = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(frame);
    if(!o)
    {
        _WARNING("CANNOT LOAD %s FRAME", frame);
        return NULL;
    }

    GameSprite* ret = new GameSprite(frame);
    
    if(ret->initWithSpriteFrame(o))
    {
//        ret->_disableCocos2dAlignToCenter = true;
        ret->position(0xFFFF, 0xFFFF);
        ret->disableBilinearFilter();
        ret->scale(1.0f);
        
        
        return ret;
    }
    
    SAFE_RELEASE_OBJECT(ret);
    return NULL;
}

/**
 * return size scalated
 */
const CCSize GameSprite::size() {
    
    CCSize ret = frw::res::sprite::size();

    ret.width *= GAMESCALE;
    ret.height *= GAMESCALE;
    
    return ret;
}

/**
 * set scale
 */
void GameSprite::scale(float s) {
    
    frw::res::sprite::scale(s * GAMESCALE);
}

/**
 * return scale
 */
const float GameSprite::scale() {
    
    float ret = frw::res::sprite::scale();
    
    return ret / GAMESCALE;
}

void GameSprite::position(CCPoint pt) {
    
    sprite::position(pt);
}

void GameSprite::position(float x, float y) {
    
    position(ccp(x, y));
}

const CCPoint GameSprite::position() {
    
    return frw::res::sprite::position();
}

CCRect GameSprite::collisionBox() {

    CCRect rc;
    int w = size().width * 0.95f;
    int h = size().height * 0.95f;
    
    rc.setRect(_position.x - w / 2, _position.y - h / 2, w, h);
    
    return rc;

}


