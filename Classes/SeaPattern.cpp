//
//  SeaPattern.cpp
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 07/02/14.
//
//

#include "SeaPattern.h"
#include "SeaBubble.h"
#include "GameSprite.h"

#define kTimeToBubbles frw::math::random::Range(1.0f,2.0f)
#define kBubblesCount frw::math::random::Range(1, 3)


SeaPattern::SeaPattern(const char* framePattern, const char* frameAlternative) : FloorPattern(framePattern,frameAlternative) {
    
    _timebubble = kTimeToBubbles;
}

SeaPattern::~SeaPattern() {
    
    for(int i = _bubblesList.size()-1; i >= 0; i--) {
        
        SeaBubble* bubble = _bubblesList[i];
        SAFE_DELETE(bubble);
    }
}

void SeaPattern::addBubble() {
    
    CCPoint pt = _sprite->position();
    int w = getSize().width / 2;
    SeaBubble* bubble = new SeaBubble("Bubble_Big.png", frw::math::random::Range(pt.x - w, pt.x + w), 310, 100);
    _bubblesList.push_back(bubble);
    _manager->push(bubble->_sprite);
}

/**
 * clona questo pattern
 */
Pattern* SeaPattern::clone() {
    
    SeaPattern* ret = new SeaPattern(_framePattern.c_str(), _framePatternOverride.c_str());
    ret->initialize(_manager, _sprite->position());
    
    return ret;
}

void SeaPattern::update(float dt) {
    
    for(int i = _bubblesList.size()-1; i >= 0; i--) {
        
        SeaBubble* bubble = _bubblesList[i];
        if(bubble->update(dt, _sprite->position())) {
            SAFE_DELETE(bubble);
            _bubblesList.erase(_bubblesList.begin()+i);
        }
    }
    
    _timebubble -= dt;
    if(_timebubble < 0) {
        
        _timebubble = kTimeToBubbles;
        
        int count = kBubblesCount;
        for(int i = 0; i < count; ++i) {
            addBubble();
        }
    }
    
    FloorPattern::update(dt);
}