//
//  SceneMain.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__SceneMain__
#define __GoPenguin__SceneMain__

#include <frw/common.h>
#include "GameCommon.h"
#include "PatternHScrollManager.h"
#include "WorldPattern.h"
#include "ObstaclesManager.h"

#define kFishPosition ((480/3)-20)
#define kKeyBestScore ("FishFishFishBestScore")

class Fish;
class Counter;
class SceneMenu;
class SceneMoreGames;
class SceneGameOver;

/**
 * \class SceneMain
 * \brief Classe principale dove appare il menu e da li parte il gioco
 */
class SceneMain : public frw::core::scene, public frw::message::listener, public frw::callback::listener, public PatternHScrollListener {
    
public:
    DECLARATE_SCENE(SceneMain);
    DECLARATE_MESSAGE_LISTENER();
    DECLARATE_CALLBACK_LISTENER();
    
    /**
     * ctor
     */
    SceneMain(const char* identifier);
    
    /**
     * dtor
     */
    virtual ~SceneMain();
    
    enum Message {
        
        kMessageMoreGames,
        kMessageMoreGamesExit,
        kMessagePlay,
        kMessageEarnPoint,
        kMessageDead
        
    };
    
protected:
    /**
     * override to initialize the scene's objects
     */
    virtual void initialize();
    
    /**
     * override to finalize and delete the scene's objects
     */
    virtual void finalize();
    
    /**
     * transiton finished
     * \see frw::core::director::setNextScene
     */
    virtual void transitionFinish();
    
    /**
     * update scene's objecys
     * \param dt delta time in ms
     */
    virtual void update(float dt);

protected:
    /**
     * fai partire il gioco
     */
    void playGame();
    
    
private:
    /** patterns callback */
    virtual void onPatternsDistance(float &distance);
    
    virtual void onPatternsStopped();
    
    /** ho finito i pattern */
    virtual void onFinishedPatterns();
    
private:
    
    enum State {
        
        kIdle,
        kIntroKitten,
        kKittenWaitTouch,
        kPrepareGame,
        kGame,
        kDeadByWall,
        kWaitObstacles,
        kOhOh,
        kResult,
        kWaitReStart
        
    };
    
    State _state;
    
    float _timeToStart;
    
    // punti guadagnati
    int _currentPoint;
    
    /**< punteggio migliore */
    int _bestScore;
    
    /** mondo corrente */
    int _currentWorld;
    
    /** mondi usati */
    bool _worldsUsed[256];

    /** tempo fra un pesce e l'altro */
    float _counterFishTime;
    
    /** id pesce scelto */
    int _iDFish;

    /** tempo di attesa prima che appaia un pesce */
    float _timeToFish;
    
    /** layer per colorare il background */
    frw::res::manager* _layerBackgroundColor;
    
    frw::res::manager* _layerPatterns;
    frw::res::manager* _layerPatternsWave;
    
    /** color background */
    frw::res::quad* _backgroundColor;

    frw::res::managerBatch* _batchRender;
    frw::res::managerBatch* _batchRenderWave;
    
    
    /** pattern floor background */
    PatternManager* _patterManagerFloor;
    
    /** pattern manager sky */
    PatternManager* _patterManagerSky;

    /** pattern manager sky */
    PatternManager* _patterManagerWave;

    /** ostacoli */
    ObstaclesManager* _obstaclesManager;
    
    /** dito */
    GameSprite* _finger;
    
    GameSprite* _bolla;
    GameSprite* _bollaExplosion;
    
    /** gattino */
    Fish* _fish;
    Fish* _fishSource[3];
    
    /** counter */
    Counter* _counter;
    
    /** layer menu */
    SceneMenu* _menu;
    
    /** layer MoreGames */
    SceneMoreGames* _moreGames;
    
    /** gameover */
    SceneGameOver* _sceneGameOver;
    
    /** path alpha per mostrare il nuovo sky */
    frw::math::PathAlpha _alphaNewSkyPattern;

    /** pesci comparsa */
    std::vector<GameSprite*> _fishes;
    
    frw::res::manager* _flashLayer;
    frw::res::quad* _flashDead;
};

#endif /* defined(__GoPenguin__SceneMain__) */
