//
//  Obstacles09a.h
//  FishFishFish
//
//  Created by Stefano Campdall'Orto on 14/02/14.
//
//

#ifndef __FishFishFish__Obstacles09a__
#define __FishFishFish__Obstacles09a__

#include "Obstacles01a.h"

class Obstacles09a : public Obstacles01a {
    
public:
    Obstacles09a();
    
    /** clona questo oggetto */
    virtual Obstacles* clone();
    
};

#endif /* defined(__FishFishFish__Obstacles09a__) */
