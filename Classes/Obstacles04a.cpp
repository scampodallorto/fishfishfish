//
//  Obstacles04.cpp
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#include "Obstacles04a.h"

Obstacles04a::Obstacles04a() {
    
    _type = 4;

    CCPoint pt1 = _up->position();
    CCPoint pt2 = _down->position();
    
    int h = _up->size().height * 0.15f;
    
    pt1.y -= h*1.2f;
    pt2.y -= h*1.2f;
    
    _up->position(pt1);
    _down->position(pt2);
}

/** clona questo oggetto */
Obstacles* Obstacles04a::clone() {
    
    return new Obstacles04a();
}