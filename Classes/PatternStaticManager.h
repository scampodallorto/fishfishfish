//
//  PatternStaticManager.h
//  GoPenguin
//
//  Created by Stefano Campdall'Orto on 06/02/14.
//
//

#ifndef __GoPenguin__PatternStaticManager__
#define __GoPenguin__PatternStaticManager__

#include <frw/common.h>

#include "Pattern.h"
#include "PatternManager.h"

/**
 * \class PatternManager
 * \brief pattern manager statico, riempie orizzotalmente con il pattern che si inserisce
 */
class PatternStaticManager : public PatternManager {
    
protected:
    
    /**
     * ctor
     */
    PatternStaticManager(frw::res::managerBatch* managerBatch, Pattern* pattern, float height = -1);
    
    /**
     * aggiorna pattern statico e riempi lo schermo
     */
    virtual void update(float dtime) {}
    
public:
    static PatternStaticManager* create(frw::res::managerBatch* managerBatch, Pattern* pattern, float height = -1) {
        
        return new PatternStaticManager(managerBatch, pattern, height);
        
    }
};

#endif /* defined(__GoPenguin__PatternStaticManager__) */
