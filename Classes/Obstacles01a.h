//
//  Obstacles01.h
//  FlappyKitten
//
//  Created by Stefano Campdall'Orto on 10/02/14.
//
//

#ifndef __FlappyKitten__Obstacles01__
#define __FlappyKitten__Obstacles01__

#include "Obstacles.h"
#include "GameSprite.h"

class Obstacles01a : public Obstacles {
    
public:
    friend class ObstaclesManager;
    
    Obstacles01a();
    
    virtual ~Obstacles01a();
    
    /** inserisci nella scena */
    virtual void push(frw::res::manager* manager);
    
    /** l'ostacolo e' stato toccato da sto sprite? */
    virtual bool isTouch(const CCRect &rect);
    
    /** sposta ostacoli */
    virtual bool update(float ftime);

    /** clona questo oggetto */
    virtual Obstacles* clone();
    
    /** nascondi o mostra l'ostacolo */
    virtual void visible(bool enable);

    virtual CCPoint position();
    
    virtual CCSize size();
    
protected:
    float _speed;
    
    float _x;
    
    bool _earned;
        
    GameSprite* _up;
    
    GameSprite* _down;
    
    CCRect _rectUp;
    
    CCRect _rectDown;

};

#endif /* defined(__FlappyKitten__Obstacles01__) */
