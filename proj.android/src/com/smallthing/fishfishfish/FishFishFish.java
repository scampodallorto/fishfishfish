/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.smallthing.tinyfish;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxEditText;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.lib.Cocos2dxRenderer;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;

import com.google.ads.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.util.Log;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.app.Activity;
import android.widget.LinearLayout;

import com.smallthing.platform.Platform;
import com.smallthing.chartboost.ChartboostEx;
import com.chartboost.sdk.*;
import com.google.ads.*;

public class FishFishFish extends Cocos2dxActivity{
	
    //------------------------------------------------------------------------------------------------------------------------------
    // admob
    private AdView mAdView;
    private static final boolean ENABLE_ADMOB = true;
    
    private Cocos2dxGLSurfaceView mGLView;
    private static final String TAG = "FishFishFish";
    
    //------------------------------------------------------------------------------------------------------------------------------
    // c++->java
    private native void onFbPostError();
    private native void onFbPostDone();
    private native String onFbPostMessage();
    private native String onFbPostCaption();
    
    private static Object activity; //inited after onCreated
    private Context context;
    
	protected void onCreate(Bundle savedInstanceState){
        
		super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        mGLView = (Cocos2dxGLSurfaceView) findViewById(R.id.game_gl_surfaceview);
        
        mGLView.setEGLConfigChooser(5, 6, 5, 0, 16, 0);
        
        // tieni attivo lo schermo, per il tilt
        mGLView.setKeepScreenOn(true);
        
        mGLView.setEGLContextClientVersion(2);
        
        mGLView.setCocos2dxRenderer(new Cocos2dxRenderer());
        
        activity = this;
        
        context = getApplicationContext();
        Platform._game_context = context;
        
        //----------------------------------------------------------------------------------------
        //@ SWARM
        Platform.onCreate(getApplicationContext(), this, TAG);
        Platform.SocialSystem_SwarmConnectWithGuest(SwarmConsts.App.APP_ID, SwarmConsts.App.APP_AUTH);
        
        //----------------------------------------------------------------------------------------
        // ADMOB
        // Look up the AdView as a resource and load a request.
        //AdView adView = (AdView)this.findViewById(R.id.adView);
        //adView.loadAd(new AdRequest());
	}
    
    public static Object getInstance(){
        
        Log.i(TAG, "getInstance()");
        return activity;
    }
    
    public void onResume() {
        super.onResume();
        
        Platform.SocialSystem_SwarmResume();
    }
    
    public void onPause() {
        super.onPause();
        
        Platform.SocialSystem_SwarmPause();
    }
    
    
	@Override
	protected void onStart() {
		super.onStart();
	}
    
	@Override
	protected void onStop() {
		super.onStop();
	}
    
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
    
	@Override
	public void onBackPressed() {
        super.onBackPressed();
	}
    
    static {
        System.loadLibrary("cocos2dcpp");
    }
}
