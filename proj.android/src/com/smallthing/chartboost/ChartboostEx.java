package com.smallthing.chartboost;

import android.util.Log;
import android.content.Context;
import android.view.View;
import android.view.View.OnLayoutChangeListener;

import android.content.Context;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.app.Activity;
import android.widget.Toast;

import com.smallthing.platform.Platform;

import com.chartboost.sdk.*;

public class ChartboostEx {

    static private Activity _activity;
    private static String _TAG;
    static private Chartboost _cb;
    static private ChartboostEx _instance;
    
    //@ get instance
    static public ChartboostEx getInstance() {
        
        return _instance;
    }
    
    //@ costruisci
    public ChartboostEx(Activity activity, String TAG) {
        
        ChartboostEx._activity = activity;
        ChartboostEx._TAG = TAG;
        _instance = this;
        
    }
    
    //@ crea chartboost
    public void onCreate(String YOUR_APP_ID, String YOUR_APP_SIGNATURE) {
        
        // Configure Chartboost
        this._cb = Chartboost.sharedChartboost();
        String appId = YOUR_APP_ID;
        String appSignature = YOUR_APP_SIGNATURE;
        
        this._cb.onCreate(ChartboostEx._activity, appId, appSignature, ChartboostEx._chartBoostDelegate);
        
        // Notify the beginning of a user session
        this._cb.startSession();

    }
    
    public void onCreate(Chartboost cb) {
        
        this._cb = cb;
    }

    public void onStart() {
        
        this._cb.onStart(ChartboostEx._activity);
    }
    
    public void onStop() {
    
        this._cb.onStop(ChartboostEx._activity);
    }
    
    public void onDestroy() {
        
        this._cb.onDestroy(ChartboostEx._activity);
    }
    
    public boolean onBackPressed() {
 
        // If an interstitial is on screen, close it. Otherwise continue as normal.
        if (this._cb.onBackPressed())
            return true;
        return false;
    }
    
    public void ShowInterestitial() {
        
        // mostra interestitial
        this._cb.showInterstitial();
    }

    public void ShowMoreApps() {
        
        // mostra interestitial
        this._cb.showMoreApps();
    }
    
    //------------------------------------------------------------------------------------
    // DELEGATE
    static public ChartboostDelegate _chartBoostDelegate = new ChartboostDelegate() {
    
            /*
             * Chartboost delegate methods
             *
             * Implement the delegate methods below to finely control Chartboost's behavior in your app
             *
             * Minimum recommended: shouldDisplayInterstitial()
             */
                
            
            /*
             * shouldDisplayInterstitial(String location)
             *
             * This is used to control when an interstitial should or should not be displayed
             * If you should not display an interstitial, return false
             *
             * For example: during gameplay, return false.
             *
             * Is fired on:
             * - showInterstitial()
             * - Interstitial is loaded & ready to display
             */
            @Override
                public boolean shouldDisplayInterstitial(String location) {
                Log.i(ChartboostEx._TAG, "SHOULD DISPLAY INTERSTITIAL '"+location+ "'?");
                return true;
            }

            /*
             * shouldRequestInterstitial(String location)
             *
             * This is used to control when an interstitial should or should not be requested
             * If you should not request an interstitial from the server, return false
             *
             * For example: user should not see interstitials for some reason, return false.
             *
             * Is fired on:
             * - cacheInterstitial()
             * - showInterstitial() if no interstitial is cached
             *
             * Notes:
             * - We do not recommend excluding purchasers with this delegate method
             * - Instead, use an exclusion list on your campaign so you can control it on the fly
             */
            @Override
            public boolean shouldRequestInterstitial(String location) {
                Log.i(ChartboostEx._TAG, "SHOULD REQUEST INSTERSTITIAL '"+location+ "'?");
                return true;
            }

            /*
             * didCacheInterstitial(String location)
             *
             * Passes in the location name that has successfully been cached
             *
             * Is fired on:
             * - cacheInterstitial() success
             * - All assets are loaded
             *
             * Notes:
             * - Similar to this is: cb.hasCachedInterstitial(String location)
             * Which will return true if a cached interstitial exists for that location
             */
            @Override
            public void didCacheInterstitial(String location) {
                Log.i(ChartboostEx._TAG, "INTERSTITIAL '"+location+"' CACHED");
            }

            /*
             * didFailToLoadInterstitial(String location)
             *
             * This is called when an interstitial has failed to load for any reason
             *
             * Is fired on:
             * - cacheInterstitial() failure
             * - showInterstitial() failure if no interstitial was cached
             *
             * Possible reasons:
             * - No network connection
             * - No publishing campaign matches for this user (go make a new one in the dashboard)
             */
            @Override
            public void didFailToLoadInterstitial(String location) {
            // Show a house ad or do something else when a chartboost interstitial fails to load

                Log.i(ChartboostEx._TAG, "INTERSTITIAL '"+location+"' REQUEST FAILED");
                //Toast.makeText(ChartboostEx._activity, "Interstitial '"+location+"' Load Failed",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didDismissInterstitial(String location)
             *
             * This is called when an interstitial is dismissed
             *
             * Is fired on:
             * - Interstitial click
             * - Interstitial close
             *
             * #Pro Tip: Use the delegate method below to immediately re-cache interstitials
             */
            @Override
            public void didDismissInterstitial(String location) {

                // Immediately re-caches an interstitial
                ChartboostEx._cb.cacheInterstitial(location);

                Log.i(ChartboostEx._TAG, "INTERSTITIAL '"+location+"' DISMISSED");
                //Toast.makeText(ChartboostEx._activity, "Dismissed Interstitial '"+location+"'",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didCloseInterstitial(String location)
             *
             * This is called when an interstitial is closed
             *
             * Is fired on:
             * - Interstitial close
             */
            @Override
            public void didCloseInterstitial(String location) {
                Log.i(ChartboostEx._TAG, "INSTERSTITIAL '"+location+"' CLOSED");
                //Toast.makeText(ChartboostEx._activity, "Closed Interstitial '"+location+"'",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didClickInterstitial(String location)
             *
             * This is called when an interstitial is clicked
             *
             * Is fired on:
             * - Interstitial click
             */
            @Override
            public void didClickInterstitial(String location) {
                Log.i(ChartboostEx._TAG, "DID CLICK INTERSTITIAL '"+location+"'");
                //Toast.makeText(ChartboostEx._activity, "Clicked Interstitial '"+location+"'",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didShowInterstitial(String location)
             *
             * This is called when an interstitial has been successfully shown
             *
             * Is fired on:
             * - showInterstitial() success
             */
            @Override
            public void didShowInterstitial(String location) {
                Log.i(ChartboostEx._TAG, "INTERSTITIAL '" + location + "' SHOWN");
            }

            /*
             * didFailToLoadURL(String location)
             *
             * This is called when a url after a click has failed to load for any reason
             *
             * Is fired on:
             * - Interstitial click
             * - More-Apps click
             *
             * Possible reasons:
             * - No network connection
             * - no valid activity to launch
             * - unable to parse url
             */
            @Override
            public void didFailToLoadUrl(String url) {
            // Show a house ad or do something else when a chartboost interstitial fails to load

                Log.i(ChartboostEx._TAG, "URL '"+url+"' REQUEST FAILED");
                //Toast.makeText(ChartboostEx._activity, "URL '"+url+"' Load Failed",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * More Apps delegate methods
             */

            /*
             * shouldDisplayLoadingViewForMoreApps()
             *
             * Return false to prevent the pretty More-Apps loading screen
             *
             * Is fired on:
             * - showMoreApps()
             */
            @Override
            public boolean shouldDisplayLoadingViewForMoreApps() {
                return true;
            }

            /*
             * shouldRequestMoreApps()
             *
             * Return false to prevent a More-Apps page request
             *
             * Is fired on:
             * - cacheMoreApps()
             * - showMoreApps() if no More-Apps page is cached
             */
            @Override
            public boolean shouldRequestMoreApps() {

                return true;
            }

            /*
             * shouldDisplayMoreApps()
             *
             * Return false to prevent the More-Apps page from displaying
             *
             * Is fired on:
             * - showMoreApps()
             * - More-Apps page is loaded & ready to display
             */
            @Override
            public boolean shouldDisplayMoreApps() {
                Log.i(ChartboostEx._TAG, "SHOULD DISPLAY MORE APPS?");
                return true;
            }

            /*
             * didFailToLoadMoreApps()
             *
             * This is called when the More-Apps page has failed to load for any reason
             *
             * Is fired on:
             * - cacheMoreApps() failure
             * - showMoreApps() failure if no More-Apps page was cached
             *
             * Possible reasons:
             * - No network connection
             * - No publishing campaign matches for this user (go make a new one in the dashboard)
             */
            @Override
            public void didFailToLoadMoreApps() {
                Log.i(ChartboostEx._TAG, "MORE APPS REQUEST FAILED");
                //Toast.makeText(ChartboostEx._activity, "More Apps Load Failed",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didCacheMoreApps()
             *
             * Is fired on:
             * - cacheMoreApps() success
             * - All assets are loaded
             */
            @Override
            public void didCacheMoreApps() {
                Log.i(ChartboostEx._TAG, "MORE APPS CACHED");
            }

            /*
             * didDismissMoreApps()
             *
             * This is called when the More-Apps page is dismissed
             *
             * Is fired on:
             * - More-Apps click
             * - More-Apps close
             */
            @Override
            public void didDismissMoreApps() {
                Log.i(ChartboostEx._TAG, "MORE APPS DISMISSED");
                //Toast.makeText(ChartboostEx._activity, "Dismissed More Apps",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didCloseMoreApps()
             *
             * This is called when the More-Apps page is closed
             *
             * Is fired on:
             * - More-Apps close
             */
            @Override
            public void didCloseMoreApps() {
                Log.i(ChartboostEx._TAG, "MORE APPS CLOSED");
                //Toast.makeText(ChartboostEx._activity, "Closed More Apps",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didClickMoreApps()
             *
             * This is called when the More-Apps page is clicked
             *
             * Is fired on:
             * - More-Apps click
             */
            @Override
            public void didClickMoreApps() {
                Log.i(ChartboostEx._TAG, "MORE APPS CLICKED");
                //Toast.makeText(ChartboostEx._activity, "Clicked More Apps",
                //Toast.LENGTH_SHORT).show();
            }

            /*
             * didShowMoreApps()
             *
             * This is called when the More-Apps page has been successfully shown
             *
             * Is fired on:
             * - showMoreApps() success
             */
            @Override
            public void didShowMoreApps() {
                Log.i(ChartboostEx._TAG, "MORE APPS SHOWED");
            }

            /*
             * shouldRequestInterstitialsInFirstSession()
             *
             * Return false if the user should not request interstitials until the 2nd startSession()
             *
             */
            @Override
            public boolean shouldRequestInterstitialsInFirstSession() {
                return true;
            }
        };

}

