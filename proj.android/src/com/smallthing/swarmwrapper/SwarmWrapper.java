
package com.smallthing.swarmwrapper;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.util.Log;
import android.content.Context;

import com.smallthing.platform.Platform;

import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmActivity;
import com.swarmconnect.SwarmActiveUser;
import com.swarmconnect.SwarmLeaderboard;
import com.swarmconnect.SwarmLeaderboardScore;
import com.swarmconnect.SwarmLeaderboard.SubmitScoreCB;
import com.swarmconnect.SwarmAchievement;
import com.swarmconnect.SwarmAchievement.GotAchievementsListCB;
import com.swarmconnect.SwarmAchievement.AchievementUnlockedCB;

public class SwarmWrapper {
    
    private static Activity _activity; //inited after onCreated
    private static SwarmActiveUser _swarm_user;
    private static SwarmAchievement _swarm_ach;
    private static boolean _swarmconnected = false;
    private static int _codeApp;
    private static String _keyApp;
    private static String TAG;
    private static SwarmWrapper _handle;
    private static SwarmLeaderboard _handleLearderboard;
    
    public void onCreate(Activity activity, int codeApp, String keyApp, String tagGame)
    {
        _activity  = activity;
        _codeApp = codeApp;
        _keyApp = keyApp;
        SwarmWrapper.TAG = tagGame;
        
        Swarm.setActive(_activity);
        Swarm.setAllowGuests(false);
        Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER Swarm.setActive >>");
        
        _handle = this;
        
        
        if ( Swarm.isEnabled() ) {
            
            Swarm.init(_activity, _codeApp, _keyApp, SwarmWrapper._mySwarmLoginListener);
            Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER Swarm.init >>");
            
            _swarm_ach = new SwarmAchievement();
            
            SwarmAchievement.getAchievementsList(new com.swarmconnect.SwarmAchievement.GotAchievementsListCB() {
                
                @Override
                public void gotList(java.util.List<SwarmAchievement> achievements)
                {
                    for (int i = 0; i < achievements.size(); i++) {
                        
                        SwarmAchievement ach = achievements.get(i);
                        
                        Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER ACH: " + ach.title + " >>");
                    }
                    
                    Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER ACH End >>");
                }
                
            });
        }

    }

    public void onCreateWithGuest(Activity activity, int codeApp, String keyApp, String tagGame)
    {
        _activity  = activity;
        _codeApp = codeApp;
        _keyApp = keyApp;
        SwarmWrapper.TAG = tagGame;
        
        Swarm.setActive(_activity);
        Swarm.setAllowGuests(true);
        Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER Swarm.setActive >>");
        
        _handle = this;
    }
    
    public void onResume() {
        
        Swarm.setActive(_activity);
    }
    
    public void onPause() {
        
        Swarm.setInactive(_activity);
    }
    
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    // SWARM LISTENER
    private native void onSwarmLoginStarted();
    private native void onSwarmLoginCanceled();
    private native String onSwarmUserLoggedIn();
    private native String onSwarmUserLoggedOut();
    private native void onSwarmAchievementsUnLock(int id);
    private native void onSwarmAchievementsError(int id);
    
    private static com.swarmconnect.delegates.SwarmLoginListener _mySwarmLoginListener = new com.swarmconnect.delegates.SwarmLoginListener() {
    
        // This method is called when the login process has started
        // (when a login dialog is displayed to the user).
        public void loginStarted() {
    
            Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER loginStarted() >>");
            
            SwarmWrapper._handle.onSwarmLoginStarted();
        }

        // This method is called if the user cancels the login process.
        public void loginCanceled() {

            _swarmconnected = false;
            Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER loginCanceled() >>");
            
            SwarmWrapper._handle.onSwarmLoginCanceled();
        }

        // This method is called when the user has successfully logged in.
        public void userLoggedIn(SwarmActiveUser user)
        {
            _swarmconnected = true;
            Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER userLoggedIn() >>");
            _swarm_user = user;
            
            SwarmWrapper._handle.onSwarmUserLoggedIn();

        }

        // This method is called when the user logs out.
        public void userLoggedOut()
        {
            Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER userLoggedOut() >>");
            _swarmconnected = false;
            
            SwarmWrapper._handle.onSwarmUserLoggedOut();
        }
    };
    
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    public void Connect() {

        Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER Connect() >>");

// per AMAZON
        // Swarm.enableAlternativeMarketCompatability();


        if(!Swarm.isInitialized())
        {
            Swarm.init(_activity, _codeApp, _keyApp, SwarmWrapper._mySwarmLoginListener);
            Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER Swarm.init >>");
            
            _swarm_ach = new SwarmAchievement();

            SwarmAchievement.getAchievementsList(new com.swarmconnect.SwarmAchievement.GotAchievementsListCB() {
                
                @Override
                public void gotList(java.util.List<SwarmAchievement> achievements) 
                {
                    for (int i = 0; i < achievements.size(); i++) {
                        
                        SwarmAchievement ach = achievements.get(i);
                        
                        Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER ACH: " + ach.title + " >>");
                    }
                    
                    Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER ACH End >>");
                }
                
            });
        }

        //if(!Swarm.isLoggedIn() && Swarm.isInitialized() && Swarm.isEnabled())
        //    Swarm.showLogin();
    }

    // connette una leaderboard per poterla tracciare
    public void onCreateLeaderboard(int LEADERBOARD_ID) {

        SwarmLeaderboard.getLeaderboardById(LEADERBOARD_ID, new SwarmLeaderboard.GotLeaderboardCB()
        {
            @Override
            public void gotLeaderboard(SwarmLeaderboard leaderboard)
            {
                SwarmWrapper._handleLearderboard = leaderboard;
            }
        });
    }

    public void ShowLeaderboard(int LEADERBOARD_ID) {

        SwarmLeaderboard.showLeaderboard(LEADERBOARD_ID);
    }

    private native void onSwarmSetScoreLeaderboardFailed();
    private native void onSwarmSetScoreLeaderboardDone();

    private static String payloadData;
    public void SetScoreLeaderboard(int LEADERBOARD_ID, int score) {

        if(!Swarm.isLoggedIn())
        {
            onSwarmSetScoreLeaderboardFailed();
            return;
        }

        SwarmLeaderboard.submitScore(LEADERBOARD_ID, score, payloadData, new SubmitScoreCB() {

        @Override
            public void scoreSubmitted(int rank) {
                // Score has been submitted
                onSwarmSetScoreLeaderboardDone();
            }
            });
    }

    private native void onSwarmLeaderboardGetRank(int id);

    public static void SocialSystem_LeaderboardRequestRank(int id) {

        Log.i(SwarmWrapper.TAG, "<< SocialSystem_LeaderboardRequestRank >>");

        if(SwarmWrapper._handleLearderboard == null)
        {
            SwarmWrapper._handle.onSwarmLeaderboardGetRank(9999);
            Log.i(SwarmWrapper.TAG, "<< ERRORE: CONNETTERE LEADERBOARD PER POTER CHIAMARE (SocialSystem_LeaderboardRequestRank) >>");
            return;
        }

        SwarmWrapper._handleLearderboard.getScoreForUser(SwarmWrapper._handle._swarm_user, new SwarmLeaderboard.GotScoreCB() {

            @Override
            public void gotScore(SwarmLeaderboardScore score) {

                Log.i(SwarmWrapper.TAG, "<< SocialSystem_LeaderboardRequestRank -> gotScore >>");

                if(score != null)
                {
                    int rank = score.rank;
                    SwarmWrapper._handle.onSwarmLeaderboardGetRank(rank);
                }
                else
                {
                    Log.i(SwarmWrapper.TAG, "<< ERROR SocialSystem_LeaderboardRequestRank -> gotScore NULL >>");
                    SwarmWrapper._handle.onSwarmLeaderboardGetRank(9999);
                }
            }
        });
    }

    private native void onSwarmLeaderboardGetFirstRank(float value, java.lang.String avatarName);

// connette una leaderboard per poterla tracciare
    public void SocialSystem_LeaderboardRequestFirstRank(int LEADERBOARD_ID) {

        SwarmLeaderboard.getLeaderboardById(LEADERBOARD_ID, new SwarmLeaderboard.GotLeaderboardCB()
        {
            @Override
            public void gotLeaderboard(SwarmLeaderboard leaderboard)
            {
                leaderboard.getTopScores(SwarmLeaderboard.DateRange.ALL, new SwarmLeaderboard.GotScoresCB() {

                    @Override
                    public void gotScores(int pageNum, java.util.List<SwarmLeaderboardScore> scores) {

                        if(pageNum < 0) {

                            Log.i(SwarmWrapper.TAG, "<< SocialSystem_LeaderboardRequestFirstRank -> NO SCORE FOUND >>");
                            return;
                        }

                        if(pageNum == 0 && scores.size() > 0) {

                            SwarmLeaderboardScore score = scores.get(0);
                            Log.i(SwarmWrapper.TAG, "<< SocialSystem_LeaderboardRequestFirstRank -> PAGE 0 >>");

                            if(score != null)
                            {
                                float value = score.score;
                                java.lang.String avatarName = score.user.username;
                                Log.i(SwarmWrapper.TAG, "<< SocialSystem_LeaderboardRequestFirstRank: " + value + " name: " + avatarName + ">>");
                                SwarmWrapper._handle.onSwarmLeaderboardGetFirstRank(value, avatarName);
                            }
                        }
                    }
                });
            }
        });
    }

    public static void SocialSystem_AchievementsReset() {

    }

    public static void SocialSystem_AchievementsShow() {

        Swarm.showAchievements();
    }

    private static int LAST_ACHIEVEMENT_ID = -1;
    
    public static void SocialSystem_AchievementsSubmit(int ACHIEVEMENT_ID) {

        if(!Swarm.isLoggedIn())
        {
            SwarmWrapper._handle.onSwarmAchievementsError(ACHIEVEMENT_ID);
            return;
        }

        LAST_ACHIEVEMENT_ID = ACHIEVEMENT_ID;
        SwarmAchievement.unlock(ACHIEVEMENT_ID, new AchievementUnlockedCB() {

            @Override
            public void achievementUnlocked(boolean unlocked, java.util.Date unlockedDate) {
                
                if (unlocked) {
                    // Achievement was unlocked by this call
                    Log.i(SwarmWrapper.TAG, "<< SWARMWRAPPER ACH: " + unlockedDate + " >>");
                    SwarmWrapper._handle.onSwarmAchievementsUnLock(LAST_ACHIEVEMENT_ID);
                    
                } else {
                    // Achievement was not unlocked by this call

                }
            }
        });
    }
    
    public void ShowLogin() {
        
        if(Swarm.isInitialized() && Swarm.isEnabled())
        {
            Swarm.showLogin();
        }
    }
    
    public void ShowDashboard() {
        
        if(_swarmconnected)
            Swarm.showDashboard();
    }
    
    public boolean IsConnectedAndLogged() {
        
        
        //return Swarm.isInitialized() && Swarm.isEnabled() && Swarm.isLoggedIn();

        return Swarm.isLoggedIn();
    }

    public void ShowLeaderboardAlways(int LEADERBOARD_ID) {

        SwarmLeaderboard.showLeaderboard(LEADERBOARD_ID, SwarmLeaderboard.DateRange.ALL);
    }


}
