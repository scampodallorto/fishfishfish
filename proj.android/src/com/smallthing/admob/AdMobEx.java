package com.smallthing.admob;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.AdListener;
import com.google.ads.Ad;

import android.util.Log;
import android.content.Context;
import android.view.View;
import android.view.View.OnLayoutChangeListener;

import android.content.Context;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.app.Activity;

import com.smallthing.platform.Platform;

/*
 
 AndroidManifest.xml:
 
 <application android:label ....
 
 <!-- ADMOB -->
 <activity android:name="com.google.ads.AdActivity"
 android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"/>
 <!-- -->
 
 
 <application/>
 
 
 layout/main.xml:
 
 <LinearLayout xmlns:ads="http://schemas.android.com/apk/lib/com.google.ads" ...
 
    ...
    <RelativeLayout ...>
        
    <!-- ADMOB -->
    <com.google.ads.AdView
        android:id="@+id/adView"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentRight="true"
        android:layout_alignParentBottom="true"
        ads:adUnitId="a1513fadd2f35bc"
        ads:adSize="BANNER"
        ads:testDevices="DEVICE ID PER TEST"/>
        <!-- se si abilita questo, appare subito l'iAD ads:loadAdOnCreate="true"/> -->
    <!-- -->
 
        ...

 </RelativeLayout>
 
 
 file <project>.java:
 
 //----------------------------------------------------------------------------------------
 //@ ADMOB

 private AdMobEx _admob;
 
 protected void onCreate(Bundle savedInstanceState){
 ...
 _admob = new AdMobEx();
 _admob.onCreate(OZ.TAG, this, this.findViewById(R.id.adView));
...
 }
 
 @Override
 public void onDestroy() {
 
 // Destroy the AdView.
 if (_admob != null) {
 _admob.onDestroy();
 }
 
 super.onDestroy();
 }
 
 */


public class AdMobEx {

    //------------------------------------------------------------------------------------------------------------------------------
    //@ ADBMOB
    static private AdView _view;

    //@ Context game
    private Activity _activity;
    
    //@ lof tag
    static public String TAG;
    
    //@ static enable
    static private AdMobEx _handle;
    
    static public AdMobEx getInstance() {
        
        return _handle;
    }
    
    private void _create(View view) {
        
        // single handle
        _handle = this;
        
        //------------------------------------------------------------------------------------------------------------------------------
        //@ ADBMOB
        //@ * per abilitarlo, decommentare in res/layout e AndroidManifest.xml il codice relativo ad admob
        //@
        _view = (AdView)view; // this.findViewById(R.id.adView);
        _view.setVisibility(AdView.VISIBLE);
        
        _view.addOnLayoutChangeListener(new OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(final View pV, final int pLeft, final int pTop, final int pRight, final int pBottom, final int pOldLeft, final int pOldTop, final int pOldRight, final int pOldBottom) {
                final float lAdHeight = _view.getHeight();
                if (lAdHeight == 0) {
                    Log.i(AdMobEx.TAG, ">>>> _view.onLayoutChange(...) _view.height='" + lAdHeight + "'. AdMob returned an empty ad !");
                    // Show custom ads
                    
                    Platform.setAdViewHeight(0);
                    
                } else {
                    Log.i(AdMobEx.TAG, ">>>> _view.onLayoutChange(...) _view.height='" + lAdHeight + "'");
                    // Make AdView visible
                    
                    Platform.setAdViewHeight((int)lAdHeight);
                    
                    Platform.System_AdViewShowed();
                }
            }
        });
        
        _view.setAdListener(new AdListener() {
            
            @Override public void onReceiveAd(Ad arg0) {
                
                // Platform.System_AdViewShowed();
            }
            
            @Override public void onPresentScreen(Ad ad) {
                
                Platform.System_AdViewPresentScreen();
            }
            
            @Override public void onDismissScreen(Ad ad) {
                
                Platform.System_DismissScreen();
            }
            
            @Override public void onLeaveApplication(Ad ad) {
                
                
            }
            
            @Override public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
                
                Log.i(AdMobEx.TAG, ">>>> AdMOB: failed to receive ad (" + error + ")");
            }
        });
    }

    //@ create class ADMOB with tester device
    public void onCreate(String TAG, Activity activity, View view, String codedevice) {
        
        _activity = activity;
        AdMobEx.TAG = TAG;
        
        _create(view);
        
        // the device code: run application with admob and in logcat get the code.
        
        AdRequest request = new AdRequest();
        request.addTestDevice(AdRequest.TEST_EMULATOR);
        //request.addTestDevice("CF1B533EB905826450CF90F7D7B1651D");
        request.addTestDevice(codedevice); 
        
    }
    
    //@ create class ADMOB without test device, for release.
    public void onCreate(String TAG, Activity activity, View view) {
        
        _activity = activity;
        AdMobEx.TAG = TAG;
        
        _create(view);
        
    }

    public void show() {
        
        _activity.runOnUiThread(new Runnable() {
            
            public void run() {
                AdMobEx._view.loadAd(new AdRequest());
            }
            
        });
    }
    
    public void onDestroy() {
        
        // Destroy the AdView.
        if (_view != null) {
            _view.destroy();
        }
            
    }
}