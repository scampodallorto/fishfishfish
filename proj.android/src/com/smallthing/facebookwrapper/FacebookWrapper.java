package com.smallthing.facebookwrapper;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.Facebook.DialogListener;

import android.util.Log;
import android.content.Context;
import android.app.Activity;

/*
 
 inside project.properties declarate:
 
 android.library.reference.2=./facebook/

 to include Facebook SDK
 
 */

public class FacebookWrapper extends Facebook{

    /**
     * TAG log
     *
     */
    
    private static final String TAG = "BubHole";
 
    /**
     * native function to implemant on your c++ project
     * 
     *
     **/
    
    private native void onFbPostError();
    private native void onFbPostDone();
    private native String onFbPostMessage();
    private native String onFbPostCaption();
    private native String onFbPostURLApplication();
    private native String onFbPostDescription();
    private native String onFbPostURLImage();
    private native String onFbPostProp1Name();
    private native String onFbPostProp1Text();
    private native String onFbPostProp1Href();

    private Activity _activity;
    private boolean _facebook_auth = false;
    private Facebook _fb;
    /**
     * @context: main application context
     *
     **/
    
    public FacebookWrapper(Activity activity, String appId) {
        super(appId);
        init(activity);
        
        _fb = this;
    }
    
    private void init(Activity activity)
    {
        _activity = activity;
        
        // TEST
        /*
        Log.i(TAG, onFbPostMessage());
        Log.i(TAG, onFbPostCaption());
        Log.i(TAG, onFbPostURLApplication());
        Log.i(TAG, onFbPostDescription());
        Log.i(TAG, onFbPostURLImage());
        */
    }
    
    /**
     * facebook permissioions
     **/
    private static final String[] FACEBOOK_PERMISSIONS = new String[] {"publish_stream", "read_stream", "offline_access"};

    /**
     * facebook post dialog
     *
     **/
    public void fb_post()
    {
        Log.v(TAG, "JNI: fb_post() called");
        Log.v(TAG, "JNI: fb_post() parameters: ");
        
        onFbPostMessage();
        onFbPostCaption();
        onFbPostURLApplication();
        onFbPostDescription();
        onFbPostURLImage();
        onFbPostProp1Name();
        onFbPostProp1Text();
        onFbPostProp1Href();
        
        _activity.runOnUiThread(new Runnable() {
            public void run() {
                
                try {
                    
                    JSONObject attachment = new JSONObject();
                    attachment.put("message", onFbPostMessage());
                    
                    if(!onFbPostCaption().equals(""))
                        attachment.put("name", onFbPostCaption());
                    
                    if(!onFbPostURLApplication().equals(""))
                        attachment.put("href", onFbPostURLApplication());
                    
                    if(!onFbPostDescription().equals(""))
                        attachment.put("description", onFbPostDescription());
                    
                    JSONObject media = new JSONObject();
                    media.put("type", "image");
                    media.put("src", onFbPostURLImage());
                    media.put("href", onFbPostURLApplication());
                    
                    attachment.put("media", new JSONArray().put(media));
                    
                    
                    if(!onFbPostProp1Text().equals("") && !onFbPostProp1Href().equals("") && !onFbPostProp1Name().equals(""))
                    {
                        JSONObject properties = new JSONObject();
                        JSONObject prop1 = new JSONObject();
                        prop1.put("text", onFbPostProp1Text());
                        prop1.put("href", onFbPostProp1Href());
                        properties.put(onFbPostProp1Name(), prop1);
                        attachment.put("properties", properties);
                    }
                    
                    /*
                    JSONObject prop2 = new JSONObject();
                    prop2.put("text", "Bubble Box on Facebook");
                    prop2.put("href", "http://www.google.com");
                    properties.put("Visit our fanpage", prop2);
                    */
                    
                    
                    Log.i(TAG, "-------POST FACEBOOK--------");
                    Log.i(TAG, attachment.toString());
                    Log.i(TAG, "----------------------------");
                    
                    Bundle parameters = new Bundle();
                    parameters.putString("attachment", attachment.toString());
                    
                    dialog(_activity, "stream.publish", parameters, new FacebookPostListener());
                }
                catch (JSONException e)
                {
                    Log.e("FACEBOOK", e.getLocalizedMessage(), e);
                }
            }
        });
    }
    
    //-------------------------------------------------------------------------------------------
    // FACEBOOK POST LISTENER
    private class FacebookPostListener implements DialogListener {
        
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null)
            {
                Log.d(TAG, "FacebookPostListener, Dialog Success! post_id=" + postId);
                
                new AsyncFacebookRunner(_fb).request(postId, new FacebookPostRequestListener());
            }
            else
            {
                _activity.runOnUiThread(new Runnable() {
                    public void run() {
                        
                        onFbPostError();
                    }
                });
            }
        }
        
        public void onCancel()
        {
            onFbPostError();
        }
        
        public void onError(DialogError e) {
            e.printStackTrace();
            onFbPostError();
        }
        
        public void onFacebookError(FacebookError e) {
            e.printStackTrace();
            onFbPostError();
        }
    }
    
    private class FacebookLoginListener implements DialogListener {
        
        public void onComplete(Bundle values) {
            if (FacebookAuthenticatedApi()) {
                
                _facebook_auth = true;
                
            } else {
                
                _facebook_auth = false;
                
            }
            if (FacebookAuthenticatedErrors()) {
                
                _facebook_auth = true;
                
            } else {
                
                _facebook_auth = false;
                
            }
        }
        
        public void onCancel() {
        }
        
        public void onError(DialogError e) {
            e.printStackTrace();
        }
        
        public void onFacebookError(FacebookError e) {
            e.printStackTrace();
        }
    }
    
    private boolean FacebookAuthenticatedApi() {
        if (!this.isSessionValid()) return false;
        try {
            Log.d("Tests", "Testing request for 'me'");
            String response = this.request("me");
            JSONObject obj = Util.parseJson(response);
            if (obj.getString("name") == null ||
                obj.getString("name").equals("")) {
                return false;
            }
            
            Log.d("Tests", "Testing graph API wall post");
            Bundle parameters = new Bundle();
            parameters.putString("message", "hello world");
            parameters.putString("description", "test test test");
            response = this.request("me/feed", parameters,
                                                     "POST");
            Log.d("Tests", "got response: " + response);
            if (response == null || response.equals("") ||
                response.equals("false")) {
                return false;
            }
            
            Log.d("Tests", "Testing graph API delete");
            response = response.replaceAll("\\{\"id\":\"", "");
            response = response.replaceAll("\"\\}", "");
            response = this.request(response, new Bundle(),
                                                     "DELETE");
            if (!response.equals("true")) return false;
            
            Log.d("Tests", "Testing old API wall post");
            parameters = new Bundle();
            parameters.putString("method", "stream.publish");
            parameters.putString("attachment",
                                 "{\"name\":\"Name=Title\"," +
                                 "\"href\":\"http://www.google.fr/\",\"" +
                                 "caption\":\"Caption\",\"description\":\"Description" +
                                 "\",\"media\":[{\"type\":\"image\",\"src\":" +
                                 "\"http://www.kratiroff.com/logo-facebook.jpg\"," +
                                 "\"href\":\"http://developers.facebook.com/\"}]," +
                                 "\"properties\":{\"another link\":{\"text\":\"" +
                                 "Facebook homepage\",\"href\":\"http://www.facebook." +
                                 "com\"}}}");;
            response = this.request(parameters);
            Log.d("Tests", "got response: " + response);
            if (response == null || response.equals("") ||
                response.equals("false")) {
                return false;
            }
            
            Log.d("Tests", "Testing wall post delete");
            response = response.replaceAll("\"", "");
            response = this.request(
                                                     response, new Bundle(), "DELETE");
            if (!response.equals("true")) return false;
            
            Log.d("Tests", "All Authenticated Tests Passed");
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }
    
    private boolean FacebookAuthenticatedErrors() {
        if (!this.isSessionValid()) return false;
        
        Log.d("Tests", "Testing that request for 'me/invalid' is rejected");
        try {
            Util.parseJson(this.request("me/invalid"));
            return false;
        } catch (Throwable e) {
            Log.d("Tests", "*" + e.getMessage() + "*");
            if (!e.getMessage().equals("Unknown path components: /invalid")) {
                return false;
            }
        }
        
        Log.d("Tests", "Testing that old API call with invalid method fails");
        Bundle params = new Bundle();
        params.putString("method", "something_invalid");
        try {
            Util.parseJson(this.request(params));
            return false;
        } catch (Throwable e) {
            Log.d("Tests", "*" + e.getMessage() + "*");
            if (!e.getMessage().equals("Unknown method") ) {
                return false;
            }
        }
        
        Log.d("Tests", "All Authenticated Error Tests Passed");
        return true;
    }
    
    private class FacebookPostRequestListener implements RequestListener {
        
        public void onComplete(final String response, final Object state) {
            Log.d(TAG, "FacebookPostRequestListener, Got response: " + response);
            try {
                JSONObject json = Util.parseJson(response);
                //final String message = json.getString("message");
                String postId = json.getString("id");
                _activity.runOnUiThread(new Runnable() {
                    public void run() {
                        
                        onFbPostDone();
                    }
                });
                
                Log.d(TAG, "FacebookPostRequestListener, wall post delete");
                if (FacebookPostDelete(postId)) {
                    _activity.runOnUiThread(new Runnable() {
                        public void run() {
                            
                            // POST DELETE
                            onFbPostError();
                        }
                    });
                } else {
                    _activity.runOnUiThread(new Runnable() {
                        public void run() {
                            
                            // POST DELETE FAILED
                            onFbPostError();
                        }
                    });
                }
            } catch (Throwable e) {
                e.printStackTrace();
                _activity.runOnUiThread(new Runnable() {
                    public void run() {
                        
                        // POST FAILED
                        onFbPostError();
                    }
                });
            }
        }
        
        public void onFacebookError(FacebookError e, final Object state) {
            e.printStackTrace();
        }
        
        public void onFileNotFoundException(FileNotFoundException e,
                                            final Object state) {
            e.printStackTrace();
        }
        
        public void onIOException(IOException e, final Object state) {
            e.printStackTrace();
        }
        
        public void onMalformedURLException(MalformedURLException e,
                                            final Object state) {
            e.printStackTrace();
        }
    }
    
    private boolean FacebookPostDelete(String postId) {
        try {
            String deleteResponse =
            this.request(postId, new Bundle(), "DELETE");
            return deleteResponse.equals("true");
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }
    
}