package com.smallthing.platform;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;

import java.lang.Exception;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.net.Uri;
import android.util.Log;
import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;

import com.smallthing.swarmwrapper.SwarmWrapper;
import com.smallthing.facebookwrapper.FacebookWrapper;
import com.smallthing.admob.AdMobEx;
import com.smallthing.chartboost.ChartboostEx;

import com.google.ads.Ad;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Platform
{
    //------------------------------------------------------------------------------------------------------------------------------
    // SWARMWRAPPER HANDLE
    private static SwarmWrapper _swarm = new SwarmWrapper();
    private static Activity _activity;
    private static String _TAG;
    public static Platform _handle;
    private static int _adViewHeight;
    
    public static void onCreate(Context context, Activity activity, String TAG)
    {
        Platform._game_context = context;
        Platform._TAG = TAG;
        Platform._activity = activity;
        _handle = new Platform();
    }   
    
    /*
     * MEMORY
     *
     */
    public static Context _game_context;
    
    public static void SocialSystem_Connect() {

        Log.i(Platform._TAG, "SocialSystem_Connect()");

        _swarm.Connect();
    }

    public static String OS_GetDeviceName()
    {
        String str = android.os.Build.MODEL;
        return str;
    }
    
    //@
    //@ GAME CENTER
    //@
    public static void SocialSystem_SwarmConnect(int id, String key)
    {

        _swarm.onCreate(_activity, id, key, _TAG);
    }   

    public static void SocialSystem_SwarmConnectWithGuest(int id, String key)
    {
        
        _swarm.onCreateWithGuest(_activity, id, key, _TAG);
    }
    
    public static void SocialSystem_SwarmResume()
    {
        _swarm.onResume();
    }

    public static void SocialSystem_SwarmPause()
    {
        _swarm.onPause();
    }
    
    public static void SocialSystem_ConnectLeaderboard(int id)
    {
        _swarm.onCreateLeaderboard(id);
    }

    public static void SocialSystem_ShowDashboard() {

        Log.i(Platform._TAG, "SocialSystem_ShowDashboard()");

        _swarm.ShowDashboard();
    }
     
    public static void SocialSystem_AuthPlayer() {

        Log.i(Platform._TAG, "SocialSystem_AuthPlayer()");

        _swarm.ShowLogin();
    }
    
    public static void SocialSystem_LeaderboardShow(int id) {

        Log.i(Platform._TAG, "SocialSystem_LeaderboardShow() " + id);
        _swarm.ShowLeaderboard(id);
    }

    public static void SocialSystem_LeaderboardShowAlways(int id) {
        
        Log.i(Platform._TAG, "SocialSystem_LeaderboardShowAlways() " + id);
        _swarm.ShowLeaderboardAlways(id);
    }
    
    public static void SocialSystem_LeaderboardReportScore(int id, int score) {

        Log.i(Platform._TAG, "SocialSystem_LeaderboardReportScore() " + id + " " + score);
        _swarm.SetScoreLeaderboard(id, score);
    }
    
    public static void SocialSystem_LeaderboardRequestRank(int id) {
        
        Log.i(Platform._TAG, "SocialSystem_LeaderboardRequestRank() " + id);
        _swarm.SocialSystem_LeaderboardRequestRank(id);
    }
    
    public static void SocialSystem_LeaderboardRequestFirstRank(int id) {
    
        Log.i(Platform._TAG, "SocialSystem_LeaderboardRequestFirstRank() " + id);
        _swarm.SocialSystem_LeaderboardRequestFirstRank(id);
    }
    
    public static boolean SocialSystem_IsConnectedAndLogged() {
        
        return _swarm.IsConnectedAndLogged();
    }
    
    public static void SocialSystem_AchievementsReset() {
        
        _swarm.SocialSystem_AchievementsReset();
    }
    
    public static void SocialSystem_AchievementsShow() {
        
        _swarm.SocialSystem_AchievementsShow();
    }
    
    public static void SocialSystem_AchievementsSubmit(int id) {
        
        Log.i(Platform._TAG, "SocialSystem_AchievementsSubmit() " + id);
        _swarm.SocialSystem_AchievementsSubmit(id);
    }
    
    //@
    //@ FACEBOOK POST
    //@
    //public static final String FACEBOOK_APP_ID = "497325310291050";
    //private static final String[] FACEBOOK_PERMISSIONS = new String[] {"publish_stream", "read_stream", "offline_access"};
    
    //------------------------------------------------------------------------------------------------------------------------------
    // FACEBOOK HANDLE
    private static FacebookWrapper _facebook; // = new FacebookWrapper(this, FACEBOOK_APP_ID);
    
    // ANSI C function
    private native String onFacebookID();
    
       
    public static void SocialSystem_FacebookAlloc()
    {
        _facebook = new FacebookWrapper(_activity, _handle.onFacebookID());
    }
    
    public static void SocialSystem_FacebookPost()
    {
        _facebook.fb_post();
    }
    

    //@
    //@ MEMORY INFO
    //@
    public static int System_MemoryUsed()
    {
        return -1;
    }
    
    public static int System_MemoryFree()
    {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) _game_context.getSystemService(_game_context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem; // / 1048576L;
        return (int)availableMegs;
    }
    
    public static int System_MemoryTotal()
    {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) _game_context.getSystemService(_game_context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem; // / 1048576L;
        return (int)availableMegs;
    }

    public static void OpenUrl(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Platform._game_context.startActivity(browserIntent);
    }
    
    
    
    //--------------------------------------------------------------------------------------------
    // AdView
    //--------------------------------------------------------------------------------------------
    
    // ANSI C function to compunicate with c++
    private native void onAdViewPresentScreen();
    private native void onDismissScreen();
    private native void onAdViewShowed();
    
    public static void setAdViewHeight(int height)
    {
        _adViewHeight = height;
    }
    
    public static void System_AdViewPresentScreen() {
        
        Log.i(Platform._TAG, ">>>>> ADVIEW System_AdViewPresentScreen");
        _handle.onAdViewPresentScreen();
    }

    public static void System_AdViewShowed() {
        
        Log.i(Platform._TAG, ">>>>> ADVIEW System_AdViewShowed");
        _handle.onAdViewShowed();
    }
    
    public static void System_DismissScreen() {
        
        Log.i(Platform._TAG, ">>>>> ADVIEW System_DismissScreen");
        _handle.onDismissScreen();
    }
    
    public static int SystemiAD_height() {
        
        return _adViewHeight;
    }
    
    public static void SystemiAD_show() {
        
        AdMobEx.getInstance().show();
    }
    
    //--------------------------------------------------------------------------------------------
    // Chartboost
    //--------------------------------------------------------------------------------------------
    private native void onChartboostPresentScreen();
    
    public static void System_ChartboostDisplayInterstitial() {
        
        Log.i(Platform._TAG, ">>>>> CHARTBOOST System_ChartboostDisplayInterstitial");
        _handle.onChartboostPresentScreen();
    }
    
    public static void System_ChartboostShowInterestitial() {
        
        ChartboostEx.getInstance().ShowInterestitial();
    }

    public static void System_ChartboostShowMoreGames() {
        
        ChartboostEx.getInstance().ShowMoreApps();
    }
    
    //--------------------------------------------------------------------------------------------
    // internet connection
    // to use enable: <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    //--------------------------------------------------------------------------------------------
    public static int System_CheckInternetConnection() {
        
        ConnectivityManager cm =
        (ConnectivityManager) _game_context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return 1;
        }
        return -1;
    }

    
    //--------------------------------------------------------------------------------------------
    // Open Web Page inside activity
    //--------------------------------------------------------------------------------------------
    public static void System_OpenWebPage(String link) {
        
        Bundle mBundle = new Bundle();
        mBundle.putString("link", link);
        
        Intent intent = new Intent(_activity, AppsPromoActivity.class);
        intent.putExtras(mBundle);
        intent.putExtra("URL", link);
        _activity.startActivityForResult(intent, 0);
    }

    public static void System_OpenWebPageHTML(String code) {
        
    }
}




