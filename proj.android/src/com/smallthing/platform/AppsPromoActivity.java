package com.smallthing.platform;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;

import java.lang.Exception;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.net.Uri;
import android.util.Log;
import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AlertDialog;

import android.net.MailTo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.content.DialogInterface;
import android.view.Window;
import android.view.WindowManager;

public class AppsPromoActivity extends Activity {
    
    private static String _openWebPageLink = "";
	private static Activity main;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /*
         if(savedInstanceState == null) {
         
         Log.i("AppsPromoActivity", ">>>>> Bundle == NULL");
         
         return;
         }
         
         String link = savedInstanceState.getString("link");
         */
		main = this;
        Intent intent = getIntent();
        String Url = intent.getStringExtra("URL");
        _openWebPageLink = Url;
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        runOnUiThread(new Runnable() {
            public void run() {
                
                try {
                    
                    WebView theWebPage = new WebView(AppsPromoActivity.this);
                    WebSettings settings = theWebPage.getSettings();
                    settings.setJavaScriptEnabled(true);
                    theWebPage.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                    
                    final AlertDialog alertDialog = new AlertDialog.Builder(AppsPromoActivity.this).create();
                    
                    final ProgressDialog progressBar = ProgressDialog.show(AppsPromoActivity.this, "Loading...", "");
                    
                    theWebPage.setWebViewClient(new WebViewClient() {
                        
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                        
                        public void onPageFinished(WebView view, String url) {
                            Log.i("AppsPromoActivity", "Finished loading URL: " +url);
                            if (progressBar.isShowing()) {
                                progressBar.dismiss();
                            }
                        }
                        
                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                            Log.e("AppsPromoActivity", "Error: " + description);
                            alertDialog.setTitle("Error");
                            alertDialog.setMessage(description);
                            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            });
                            alertDialog.show();
                        }
                    });
                    theWebPage.loadUrl(AppsPromoActivity._openWebPageLink);
					main.setContentView(theWebPage);
                    /*AppsPromoActivity.this.setContentView(theWebPage);
                     theWebPage.loadUrl(_openWebPageLink);*/
                }
                catch (Exception e)
                {
                    Log.e("System_OpenWebPage", e.getLocalizedMessage(), e);
                }
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        
        finish();
    }
}




