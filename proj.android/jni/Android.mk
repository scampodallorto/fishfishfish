LOCAL_LEEN_FRAMEWORK_PATH := /Users/smartmind/Works/Smallthing/LeEnGameFramework4.0
GAME_PATH := $(call my-dir)

LOCAL_PATH := $(GAME_PATH)
include $(CLEAR_VARS)
LOCAL_MODULE := cocos2dcpp_shared
LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
\
\
../../../../../LeEnGameFramework4.0/frw/assets/assets.cpp \
\
../../../../../LeEnGameFramework4.0/frw/core/director.cpp \
../../../../../LeEnGameFramework4.0/frw/core/scene.cpp \
\
../../../../../LeEnGameFramework4.0/frw/callback/callback.cpp \
\
../../../../../LeEnGameFramework4.0/frw/file/CBlowFish.cpp \
../../../../../LeEnGameFramework4.0/frw/file/CryptData.cpp \
../../../../../LeEnGameFramework4.0/frw/file/file.cpp \
\
../../../../../LeEnGameFramework4.0/frw/ipt/ipt.cpp \
\
../../../../../LeEnGameFramework4.0/frw/math/random.cpp \
../../../../../LeEnGameFramework4.0/frw/math/arc4random.c \
../../../../../LeEnGameFramework4.0/frw/math/crc.cpp \
../../../../../LeEnGameFramework4.0/frw/math/cutmullrom.cpp \
../../../../../LeEnGameFramework4.0/frw/math/spline.cpp \
../../../../../LeEnGameFramework4.0/frw/math/point.cpp \
../../../../../LeEnGameFramework4.0/frw/math/segment.cpp \
\
../../../../../LeEnGameFramework4.0/frw/message/message.cpp \
\
../../../../../LeEnGameFramework4.0/frw/res/animation.cpp \
../../../../../LeEnGameFramework4.0/frw/res/base.cpp \
../../../../../LeEnGameFramework4.0/frw/res/manager.cpp \
../../../../../LeEnGameFramework4.0/frw/res/overlay.cpp \
../../../../../LeEnGameFramework4.0/frw/res/sprite.cpp \
../../../../../LeEnGameFramework4.0/frw/res/font.cpp \
../../../../../LeEnGameFramework4.0/frw/res/fontTTF.cpp \
../../../../../LeEnGameFramework4.0/frw/res/bmfont.cpp \
../../../../../LeEnGameFramework4.0/frw/res/menu.cpp \
../../../../../LeEnGameFramework4.0/frw/res/button.cpp \
../../../../../LeEnGameFramework4.0/frw/res/gui.cpp \
../../../../../LeEnGameFramework4.0/frw/res/quad.cpp \
../../../../../LeEnGameFramework4.0/frw/res/image.cpp \
../../../../../LeEnGameFramework4.0/frw/res/parallax.cpp \
../../../../../LeEnGameFramework4.0/frw/res/progress.cpp \
\
../../../../../LeEnGameFramework4.0/frw/time/time.cpp \
\
../../../../../LeEnGameFramework4.0/frw/dsp/screen.cpp \
\
../../../../../LeEnGameFramework4.0/frw/platform/platformCommon.cpp \
../../../../../LeEnGameFramework4.0/frw/platform/platformAndroid.cpp \
\
../../../../../LeEnGameFramework4.0/frw/snd/sound.cpp \
\
../../../../../LeEnGameFramework4.0/frw/tinyxml/tinywrapper.cpp \
../../../../../LeEnGameFramework4.0/frw/tinyxml/tinyxml.cpp \
../../../../../LeEnGameFramework4.0/frw/tinyxml/tinystr.cpp \
../../../../../LeEnGameFramework4.0/frw/tinyxml/tinyxmlerror.cpp \
../../../../../LeEnGameFramework4.0/frw/tinyxml/TinyXMLHelper.cpp \
../../../../../LeEnGameFramework4.0/frw/tinyxml/tinyxmlparser.cpp \
\
../../../../../LeEnGameFramework4.0/frw/text/texts.cpp \
\
../../../../../LeEnGameFramework4.0/frw/fx/particles.cpp \
\
../../../../../LeEnGameFramework4.0/frw/game/achievements.cpp \
../../../../../LeEnGameFramework4.0/frw/game/leaderboardsafe.cpp \
../../../../../LeEnGameFramework4.0/frw/game/baloon.cpp \
../../../../../LeEnGameFramework4.0/frw/game/baloonmanager.cpp \
../../../../../LeEnGameFramework4.0/frw/game/popup.cpp \
../../../../../LeEnGameFramework4.0/frw/game/popupmanager.cpp \
../../../../../LeEnGameFramework4.0/frw/game/requestreview.cpp \
../../../../../LeEnGameFramework4.0/frw/game/credits.cpp \
../../../../../LeEnGameFramework4.0/frw/game/buttonBackLayers.cpp \
\
\
\
../../Classes/BonusPoints.cpp \
../../Classes/Counter.cpp \
../../Classes/FloorPattern.cpp \
../../Classes/GameSprite.cpp \
../../Classes/Fish.cpp \
../../Classes/FishRed.cpp \
../../Classes/FishYellow.cpp \
../../Classes/FishViolet.cpp \
../../Classes/Obstacles.cpp \
../../Classes/Obstacles01a.cpp \
../../Classes/Obstacles02a.cpp \
../../Classes/Obstacles03a.cpp \
../../Classes/Obstacles04a.cpp \
../../Classes/Obstacles05a.cpp \
../../Classes/Obstacles06a.cpp \
../../Classes/Obstacles07a.cpp \
../../Classes/Obstacles08a.cpp \
../../Classes/Obstacles09a.cpp \
../../Classes/Obstacles10a.cpp \
../../Classes/Obstacles11a.cpp \
../../Classes/Obstacles12a.cpp \
../../Classes/Obstacles13a.cpp \
../../Classes/Obstacles14a.cpp \
../../Classes/Obstacles15a.cpp \
../../Classes/ObstaclesManager.cpp \
../../Classes/Pattern.cpp \
../../Classes/PatternHScrollManager.cpp \
../../Classes/PatternManager.cpp \
../../Classes/PatternStaticManager.cpp \
../../Classes/SceneFakeToMain.cpp \
../../Classes/SceneGameOver.cpp \
../../Classes/SceneLoading.cpp \
../../Classes/SceneMain.cpp \
../../Classes/SceneMenu.cpp \
../../Classes/SceneMoreGames.cpp \
../../Classes/SeaBubble.cpp \
../../Classes/SeaPattern.cpp \
../../Classes/WorldPattern.cpp \
../../Classes/root/AppDelegate.cpp \
../../Classes/root/MyAssetsAndroid.cpp \
../../Classes/root/MyDirector.cpp \
../../Classes/root/MyScreenAndroid.cpp \
\



LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
                    $(LOCAL_PATH)/../../Classes/root \
                    $(LOCAL_LEEN_FRAMEWORK_PATH)

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
